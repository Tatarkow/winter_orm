setup_postgresql:
	./scripts/setup_postgresql.sh

setup_mongodb:
	./scripts/setup_mongodb.sh

setup: setup_postgresql setup_mongodb

clear_postgresql:
	./scripts/clear_postgresql.sh

clear_mongodb:
	./scripts/clear_mongodb.sh

clear: clear_postgresql clear_mongodb

test:
	mvn test -Duser.timezone=UTC

full_test:
	./scripts/full_test.sh
