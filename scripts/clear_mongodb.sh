#!/usr/bin/env bash

echo "Clearing MongoDB..."
mongo < ./scripts/clear_mongodb.js
echo "MongoDB has been cleared"
echo
