use winter_orm_database

db.createUser({
    user: "winter_orm_user",
    pwd: "winter_orm_password",
    roles: [ "dbOwner" ]
})
