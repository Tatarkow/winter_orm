CREATE USER winter_orm_user WITH ENCRYPTED PASSWORD 'winter_orm_password';
CREATE DATABASE winter_orm_database OWNER winter_orm_user;
GRANT ALL PRIVILEGES ON DATABASE winter_orm_database TO winter_orm_user;
