#!/usr/bin/env bash

HANDLER_FILE=./src/main/resources/META-INF/services/cz.tatarko.DatabaseHandler
CONTENT=$(cat $HANDLER_FILE)

echo "Testing H2..."
echo "cz.tatarko.H2Handler" > $HANDLER_FILE
make test
printf "H2 has been tested\n\n\n"

echo "Testing PostgreSQL..."
echo "cz.tatarko.PostgreSQLHandler" > $HANDLER_FILE
make test
printf "PostgreSQL has been tested\n\n\n"

echo "Testing MongoDB..."
echo "cz.tatarko.MongoDBHandler" > $HANDLER_FILE
make test
printf "MongoDB has been tested\n\n\n"

echo "$CONTENT" > $HANDLER_FILE
