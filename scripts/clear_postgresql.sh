#!/usr/bin/env bash

echo "Clearing PostgreSQL..."
dropdb winter_orm_database
echo "PostgreSQL has been cleared"
echo
