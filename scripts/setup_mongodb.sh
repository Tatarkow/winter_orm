#!/usr/bin/env bash

echo "Setting MongoDB..."
mongo < ./scripts/setup_mongodb.js
echo "MongoDB has been set up"
echo
