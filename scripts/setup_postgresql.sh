#!/usr/bin/env bash

echo "Setting PostgreSQL..."
psql -f ./scripts/setup_postgresql.sql
echo "PostgreSQL has been set up"
echo
