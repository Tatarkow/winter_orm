package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class TestTableObjects extends BaseTest {
    @Test
    void testEquals() throws WinterORMException {
        Author komensky1 = AuthorFactory.createKomensky();
        Author komensky2 = AuthorFactory.createKomensky();
        Author molier = AuthorFactory.createMolier();

        Assertions.assertEquals(komensky1, komensky1);

        Assertions.assertEquals(komensky1, komensky2);
        Assertions.assertEquals(komensky2, komensky1);

        Assertions.assertNotEquals(molier, komensky1);
        Assertions.assertNotEquals(komensky1, molier);
        Book othello = BookFactory.createOthello();

        Assertions.assertNotEquals(molier, othello);
        Assertions.assertNotEquals(othello, molier);
    }

    @Test
    void testToStringShakespeare() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Assertions.assertEquals("[AUTHOR] { William Shakespeare }", shakespeare.toString());
    }

    @Test
    void testRefresh() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        String newName = "Billy";

        shakespeare.set(Map.of(
                "name", newName
        ));

        Assertions.assertNotEquals(newName, shakespeare.get("name"));

        shakespeare.refresh();
        Assertions.assertEquals(newName, shakespeare.get("name"));
    }
}
