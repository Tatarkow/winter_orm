package cz.tatarko.factories;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import cz.tatarko.tables.Friend;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

public class FriendFactory extends Factory {
    public static Friend createStanislav() throws WinterORMException {
        Date birthDate = new GregorianCalendar(1996, Calendar.DECEMBER, 25).getTime();
        Author shakespeare = AuthorFactory.createShakespeare();
        Book romeo = BookFactory.createRomeo();

        return winterORM.insert(Friend.class, Map.of(
            "name", "Stanislav",
            "favoriteAuthor", shakespeare,
            "favoriteBook", romeo,
            "age", 24,
            "birthDate", birthDate
        ));
    }
}
