package cz.tatarko.factories;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.tables.Author;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

public class AuthorFactory extends Factory {
    public static Author createMolier() throws WinterORMException {
        return winterORM.insert(Author.class, Map.of(
        "name", "Molier"
        ));
    }

    public static Author createShakespeare() throws WinterORMException {
        return winterORM.insert(Author.class, Map.of(
        "name", "William",
        "surname", "Shakespeare"
        ));
    }

    public static Author createKomensky() throws WinterORMException {
        Date birthDate = new GregorianCalendar(1592, Calendar.MARCH, 28).getTime();

        return winterORM.insert(Author.class, Map.of(
            "name", "Jan",
            "middleName", "Amos",
            "surname", "Komensky",
            "birthDate", birthDate
        ));
    }
}
