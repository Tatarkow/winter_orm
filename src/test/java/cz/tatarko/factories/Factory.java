package cz.tatarko.factories;

import cz.tatarko.WinterORM;

public abstract class Factory {
    public static WinterORM winterORM;

    public static void setWinterORM(WinterORM winterORM) {
        Factory.winterORM = winterORM;
    }
}
