package cz.tatarko.factories;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;

import java.util.Map;

public class BookFactory extends Factory {
    public static Book createOthello() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();

        return winterORM.insert(Book.class, Map.of(
        "author", shakespeare,
        "title", "Othello",
        "publicationYear", 1604
        ));
    }

    public static Book createLabyrinth() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();

        return winterORM.insert(Book.class, Map.of(
        "author", komensky,
        "title", "Labyrint sveta a raj srdce"
        ));
    }

    public static Book createTartuffe() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();

        return winterORM.insert(Book.class, Map.of(
        "author", molier,
        "title", "Tartuffe",
        "publicationYear", 1664
        ));
    }

    public static Book createRomeo() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();

        return winterORM.insert(Book.class, Map.of(
        "author", shakespeare,
        "title", "Romeo a Julie",
        "publicationYear", 1595
        ));
    }
}
