package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

public class TestGet extends BaseTest {
    @Test
    void testGetList() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        Author shakespeare = AuthorFactory.createShakespeare();

        List<Author> authors = winterORM.getAll(Author.class).getList();
        Assertions.assertEquals(2, authors.size());

        Assertions.assertTrue(authors.contains(molier));
        Assertions.assertTrue(authors.contains(shakespeare));
    }

    @Test
    void testGetListDetail() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();
        Author retrievedKomensky = winterORM.getAll(Author.class).getList().get(0);

        Assertions.assertEquals(komensky.get("name"), retrievedKomensky.get("name"));
        Assertions.assertEquals(komensky.get("middleName"), retrievedKomensky.get("middleName"));
        Assertions.assertEquals(komensky.get("surname"), retrievedKomensky.get("surname"));
        Assertions.assertEquals(komensky.get("birthDate"), retrievedKomensky.get("birthDate"));
    }



    @Test
    void testGetID() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Author molier = AuthorFactory.createMolier();

        Assertions.assertNotEquals(shakespeare.getID(), molier.getID());
    }

    @Test
    void testGetOnResult() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();
        Author retrievedKomensky = winterORM.getAll(Author.class).get(komensky.getID());

        Assertions.assertEquals(komensky, retrievedKomensky);
    }

    @Test
    void testGetOnTableObject() throws WinterORMException {
        BookFactory.createLabyrinth();
        Book labyrinth = winterORM.getAll(Book.class).getList().get(0);

        String title = (String) labyrinth.get("title");
        Assertions.assertEquals("Labyrint sveta a raj srdce", title);

        Author author = (Author) labyrinth.get("author");

        String authorName = (String) author.get("name");
        Assertions.assertEquals("Jan", authorName);

        Date authorBirthDate = (Date) author.get("birthDate");
        Assertions.assertEquals("1592-03-28", formatter.format(authorBirthDate));
    }

    @Test
    void testGetOnTableObjectLoaded() throws WinterORMException, NoSuchFieldException, IllegalAccessException {
        Book tartufe = BookFactory.createTartuffe();

        Field titleField = Book.class.getDeclaredField("title");
        titleField.setAccessible(true);
        String title = (String) titleField.get(tartufe);
        Assertions.assertEquals("Tartuffe", title);

        Field authorField = Book.class.getDeclaredField("author");
        authorField.setAccessible(true);
        Author molier = (Author) authorField.get(tartufe);
        Assertions.assertTrue(molier.getLoaded());

        Field nameField = Author.class.getDeclaredField("name");
        nameField.setAccessible(true);
        String name = (String) nameField.get(molier);
        Assertions.assertEquals("Molier", name);
    }

    @Test
    void testGetOnTableObjectNotLoaded() throws WinterORMException, NoSuchFieldException, IllegalAccessException {
        BookFactory.createTartuffe();
        Book tartufe = winterORM.getAll(Book.class).getList().get(0);

        Field titleField = Book.class.getDeclaredField("title");
        titleField.setAccessible(true);
        String title = (String) titleField.get(tartufe);
        Assertions.assertEquals("Tartuffe", title);

        Field authorField = Book.class.getDeclaredField("author");
        authorField.setAccessible(true);
        Author molier = (Author) authorField.get(tartufe);
        Assertions.assertFalse(molier.getLoaded());

        Field nameField = Author.class.getDeclaredField("name");
        nameField.setAccessible(true);
        String name = (String) nameField.get(molier);
        Assertions.assertNull(name);
    }
}
