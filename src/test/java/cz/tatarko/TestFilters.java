package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.filters.DateFilter;
import cz.tatarko.filters.ForeignFilter;
import cz.tatarko.filters.IntegerFilter;
import cz.tatarko.filters.StringFilter;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class TestFilters extends BaseTest {
    @Test
    void testStringFilterEquals() throws WinterORMException {
        AuthorFactory.createShakespeare();
        Author komensky = AuthorFactory.createKomensky();
        AuthorFactory.createMolier();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("name", StringFilter.EQUALS, "Jan")
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertEquals(komensky, authors.get(0));
    }

    @Test
    void testStringFilterIsEmpty() throws WinterORMException {
        AuthorFactory.createShakespeare();
        AuthorFactory.createKomensky();
        Author molier = AuthorFactory.createMolier();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("surname", StringFilter.IS_EMPTY)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertEquals(molier, authors.get(0));
    }

    @Test
    void testStringFilterIsEmptyTrue() throws WinterORMException {
        AuthorFactory.createShakespeare();
        AuthorFactory.createKomensky();
        Author molier = AuthorFactory.createMolier();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("surname", StringFilter.IS_EMPTY, true)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertEquals(molier, authors.get(0));
    }

    @Test
    void testStringFilterIsEmptyFalse() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Author komenky = AuthorFactory.createKomensky();
        AuthorFactory.createMolier();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("surname", StringFilter.IS_EMPTY, false)
                .getList();

        Assertions.assertEquals(2, authors.size());
        Assertions.assertEquals(shakespeare, authors.get(0));
        Assertions.assertEquals(komenky, authors.get(1));
    }

    @Test
    void testIntegerFilterEquals() throws WinterORMException {
        BookFactory.createLabyrinth();
        BookFactory.createRomeo();
        String tartuffeID = BookFactory.createTartuffe().getID();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.EQUALS, 1664)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book tartuffe = winterORM.getAll(Book.class).get(tartuffeID);
        Assertions.assertEquals(tartuffe, books.get(0));
    }

    @Test
    void testIntegerFilterLessThan() throws WinterORMException {
        BookFactory.createLabyrinth();
        String othelloID = BookFactory.createOthello().getID();
        String romeoID = BookFactory.createRomeo().getID();
        BookFactory.createTartuffe();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.LESS_THAN, 1650)
                .getList();

        Assertions.assertEquals(2, books.size());

        Book othello = winterORM.getAll(Book.class).get(othelloID);
        Assertions.assertTrue(books.contains(othello));

        Book romeo = winterORM.getAll(Book.class).get(romeoID);
        Assertions.assertTrue(books.contains(romeo));
    }

    @Test
    void testIntegerFilterIsNull() throws WinterORMException {
        BookFactory.createOthello();
        String labyrinthID = BookFactory.createLabyrinth().getID();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book labyrinth = winterORM.getAll(Book.class).get(labyrinthID);
        Assertions.assertTrue(books.contains(labyrinth));
    }

    @Test
    void testIntegerFilterIsNullTrue() throws WinterORMException {
        BookFactory.createOthello();
        String labyrinthID = BookFactory.createLabyrinth().getID();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL, true)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book labyrinth = winterORM.getAll(Book.class).get(labyrinthID);
        Assertions.assertTrue(books.contains(labyrinth));
    }

    @Test
    void testIntegerFilterIsNullFalse() throws WinterORMException {
        String othelloID = BookFactory.createOthello().getID();
        BookFactory.createLabyrinth();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.IS_NULL, false)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book othello = winterORM.getAll(Book.class).get(othelloID);
        Assertions.assertTrue(books.contains(othello));
    }

    @Test
    void testIntegerFilterMoreThan() throws WinterORMException {
        BookFactory.createLabyrinth();
        BookFactory.createOthello();
        BookFactory.createRomeo();
        String tartuffeID = BookFactory.createTartuffe().getID();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.MORE_THAN, 1650)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book tartuffe = winterORM.getAll(Book.class).get(tartuffeID);
        Assertions.assertTrue(books.contains(tartuffe));
    }

    @Test
    void testDateFilterEquals() throws WinterORMException {
        AuthorFactory.createKomensky();

        Date birthDate = new GregorianCalendar(1564, Calendar.APRIL, 23).getTime();

        Author shakespeare = winterORM.insert(Author.class, Map.of(
                "name", "William",
                "surname", "Shakespeare",
                "birthDate", birthDate
        ));

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("birthDate", DateFilter.EQUALS, birthDate)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertTrue(authors.contains(shakespeare));
    }

    @Test
    void testDateFilterIsNull() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        AuthorFactory.createKomensky();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("birthDate", DateFilter.IS_NULL)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertTrue(authors.contains(molier));
    }

    @Test
    void testDateFilterIsNullTrue() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        AuthorFactory.createKomensky();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("birthDate", DateFilter.IS_NULL, true)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertTrue(authors.contains(molier));
    }

    @Test
    void testDateFilterIsNullFalse() throws WinterORMException {
        AuthorFactory.createMolier();
        Author komensky = AuthorFactory.createKomensky();

        List<Author> authors = winterORM
                .getAll(Author.class)
                .filter("birthDate", DateFilter.IS_NULL, false)
                .getList();

        Assertions.assertEquals(1, authors.size());
        Assertions.assertTrue(authors.contains(komensky));
    }

    @Test
    void testMultipleFilters() throws WinterORMException {
        BookFactory.createLabyrinth();
        String othelloID = BookFactory.createOthello().getID();
        BookFactory.createRomeo();
        BookFactory.createTartuffe();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.LESS_THAN, 1650)
                .filter("publicationYear", IntegerFilter.MORE_THAN, 1600)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book othello = winterORM.getAll(Book.class).get(othelloID);
        Assertions.assertTrue(books.contains(othello));
    }

    @Test
    void testForeignFilterEqualsFactory() throws WinterORMException {
        BookFactory.createOthello();
        Book romeo = BookFactory.createRomeo();
        BookFactory.createTartuffe();

        Author shakespeare = (Author) romeo.get("author");

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("author", ForeignFilter.EQUALS, shakespeare)
                .getList();

        Assertions.assertEquals(1, books.size());

        Book retrievedRomeo = winterORM.getAll(Book.class).get(romeo.getID());
        Assertions.assertTrue(books.contains(retrievedRomeo));
    }

    @Test
    void testForeignFilterEqualsDirectly() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();

        String othelloID = winterORM.insert(Book.class, Map.of(
                "author", shakespeare,
                "title", "Othello",
                "publicationYear", 1604
        )).getID();

        String romeoID = winterORM.insert(Book.class, Map.of(
                "author", shakespeare,
                "title", "Romeo a Julie",
                "publicationYear", 1595
        )).getID();

        BookFactory.createTartuffe();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("author", ForeignFilter.EQUALS, shakespeare)
                .getList();

        Assertions.assertEquals(2, books.size());

        Book othello = winterORM.getAll(Book.class).get(othelloID);
        Assertions.assertTrue(books.contains(othello));

        Book romeo = winterORM.getAll(Book.class).get(romeoID);
        Assertions.assertTrue(books.contains(romeo));
    }

    @Test
    void testForeignFilterCompare() throws WinterORMException {
        String othelloID = BookFactory.createOthello().getID();
        String romeoID = BookFactory.createRomeo().getID();
        BookFactory.createTartuffe();

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("author", ForeignFilter.COMPARE, "name", StringFilter.EQUALS, "William")
                .getList();

        Assertions.assertEquals(2, books.size());

        Book othello = winterORM.getAll(Book.class).related("author").get(othelloID);
        Assertions.assertTrue(books.contains(othello));

        Book romeo = winterORM.getAll(Book.class).related("author").get(romeoID);
        Assertions.assertTrue(books.contains(romeo));
    }
}
