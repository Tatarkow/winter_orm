package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.FriendFactory;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import cz.tatarko.tables.Friend;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

public class TestPrefetch extends BaseTest {
    @Test
    void testNotPrefetched() throws WinterORMException, NoSuchFieldException, IllegalAccessException {
        String stanislavID = FriendFactory.createStanislav().getID();
        Friend stanislav = winterORM.getAll(Friend.class).get(stanislavID);

        Field authorField = Friend.class.getDeclaredField("favoriteAuthor");
        authorField.setAccessible(true);
        Author author = (Author) authorField.get(stanislav);
        Assertions.assertFalse(author.getLoaded());

        Field authorNameField = Author.class.getDeclaredField("name");
        authorNameField.setAccessible(true);
        String authorName = (String) authorNameField.get(author);
        Assertions.assertNull(authorName);

        Field bookField = Friend.class.getDeclaredField("favoriteBook");
        bookField.setAccessible(true);
        Book book = (Book) bookField.get(stanislav);
        Assertions.assertFalse(book.getLoaded());

        Field bookTitleField = Book.class.getDeclaredField("title");
        bookTitleField.setAccessible(true);
        String bookTitle = (String) bookTitleField.get(book);
        Assertions.assertNull(bookTitle);
    }

    @Test
    void testPrefetched() throws WinterORMException, NoSuchFieldException, IllegalAccessException {
        String stanislavID = FriendFactory.createStanislav().getID();

        Friend stanislav = winterORM
                .getAll(Friend.class)
                .related("favoriteAuthor", "favoriteBook")
                .get(stanislavID);

        Field authorField = Friend.class.getDeclaredField("favoriteAuthor");
        authorField.setAccessible(true);
        Author author = (Author) authorField.get(stanislav);
        Assertions.assertTrue(author.getLoaded());

        Field authorNameField = Author.class.getDeclaredField("name");
        authorNameField.setAccessible(true);
        String authorName = (String) authorNameField.get(author);
        Assertions.assertEquals("William", authorName);

        Field bookField = Friend.class.getDeclaredField("favoriteBook");
        bookField.setAccessible(true);
        Book book = (Book) bookField.get(stanislav);
        Assertions.assertTrue(book.getLoaded());

        Field bookTitleField = Book.class.getDeclaredField("title");
        bookTitleField.setAccessible(true);
        String bookTitle = (String) bookTitleField.get(book);
        Assertions.assertEquals("Romeo a Julie", bookTitle);
    }
}
