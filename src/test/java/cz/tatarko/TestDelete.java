package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.filters.StringFilter;
import cz.tatarko.tables.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestDelete extends BaseTest {
    @Test
    void testDeleteOnResult() throws WinterORMException {
        AuthorFactory.createShakespeare();
        String komenskyID = AuthorFactory.createKomensky().getID();
        AuthorFactory.createMolier();

        List<Author> authorsBefore = winterORM.getAll(Author.class).getList();
        Assertions.assertEquals(3, authorsBefore.size());

        winterORM
                .getAll(Author.class)
                .filter("middleName", StringFilter.IS_EMPTY)
                .delete();

        List<Author> authorsAfter = winterORM.getAll(Author.class).getList();
        Assertions.assertEquals(1, authorsAfter.size());

        Author komensky = winterORM.getAll(Author.class).get(komenskyID);
        Assertions.assertEquals(komensky, authorsAfter.get(0));
    }

    @Test
    void testDeleteOnTableObject() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        String komenskyID = AuthorFactory.createKomensky().getID();

        List<Author> authorsBefore = winterORM.getAll(Author.class).getList();
        Assertions.assertEquals(2, authorsBefore.size());

        shakespeare.delete();

        List<Author> authorsAfter = winterORM.getAll(Author.class).getList();
        Assertions.assertEquals(1, authorsAfter.size());

        Author komensky = winterORM.getAll(Author.class).get(komenskyID);
        Assertions.assertEquals(komensky, authorsAfter.get(0));
    }
}
