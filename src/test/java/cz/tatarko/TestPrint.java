package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.tables.Author;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestPrint extends BaseTest {
    private void assertStringsSame(String expectedOutput, String output) {
        Assertions.assertEquals(
                expectedOutput.replaceAll("\\s+",""),
                output.replaceAll("\\s+","")
        );
    }

    @Test
    void testPrint() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Author molier = AuthorFactory.createMolier();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream originalStream = System.out;
        System.setOut(new PrintStream(stream));

        winterORM.getAll(Author.class).print();

        System.setOut(originalStream);

        String expectedOutput = String.format("%s\n%s\n", shakespeare, molier);
        assertStringsSame(expectedOutput, stream.toString());
    }

    @Test
    void testPrintColumns() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        Author komensky = AuthorFactory.createKomensky();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream originalStream = System.out;
        System.setOut(new PrintStream(stream));

        winterORM.getAll(Author.class).print("name", "surname");

        System.setOut(originalStream);

        String expectedOutput = "name\tsurname\n" +
                molier.get("name") + "\t" + molier.get("surname") + "\n" +
                komensky.get("name") + "\t" +  komensky.get("surname") + "\n";

        assertStringsSame(expectedOutput, stream.toString());
    }
}
