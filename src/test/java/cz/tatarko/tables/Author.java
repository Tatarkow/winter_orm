package cz.tatarko.tables;

import cz.tatarko.TableObject;
import cz.tatarko.fields.DateField;
import cz.tatarko.fields.StringField;

import java.util.Date;

public class Author extends TableObject {
    @StringField(nullable = false, maxLength = 32)
    public String name;

    @StringField(maxLength = 32)
    public String surname;

    @StringField(maxLength = 32)
    public String middleName;

    @DateField()
    public Date birthDate;

    @Override
    public String toString() {
        StringBuilder fullName = new StringBuilder("[AUTHOR] { ");

        if (name != null) {
            fullName.append(String.format("%s ", name));
        }

        if (middleName != null) {
            fullName.append(String.format("%s ", middleName));
        }

        if (surname != null) {
            fullName.append(String.format("%s ", surname));
        }

        fullName.append("}");

        return fullName.toString();
    }
}
