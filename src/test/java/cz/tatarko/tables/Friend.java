package cz.tatarko.tables;

import cz.tatarko.TableObject;
import cz.tatarko.fields.DateField;
import cz.tatarko.fields.ForeignField;
import cz.tatarko.fields.IntegerField;
import cz.tatarko.fields.StringField;

import java.util.Date;

public class Friend extends TableObject {
    @StringField()
    String name;

    @ForeignField()
    Author favoriteAuthor;

    @ForeignField()
    Book favoriteBook;

    @IntegerField(nullable = false)
    Integer age;

    @DateField(nullable = false)
    Date birthDate;
}
