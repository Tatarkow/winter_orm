package cz.tatarko.tables;

import cz.tatarko.fields.OnDelete;
import cz.tatarko.TableObject;
import cz.tatarko.fields.ForeignField;
import cz.tatarko.fields.IntegerField;
import cz.tatarko.fields.StringField;

public class Book extends TableObject {
    // TODO: Do not allow nullable=False && onDelete=SET_NULL.
    @ForeignField(nullable = false, onDelete= OnDelete.DELETE)
    public Author author;

    @StringField(nullable = false)
    public String title;

    @IntegerField()
    public Integer publicationYear;

    @Override
    public String toString() {
        return String.format("[BOOK] { %s written by %s in %s }", title, author, publicationYear);
    }
}
