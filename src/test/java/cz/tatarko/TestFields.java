package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.exceptions.validation.FieldMaxLengthExceededException;
import cz.tatarko.exceptions.validation.FieldNotNullableException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.factories.FriendFactory;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import cz.tatarko.tables.Friend;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;

public class TestFields extends BaseTest {
    @Test
    void testIntegerFieldNullableTrueInsert() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();

        winterORM.insert(Book.class, Map.of(
                "author", komensky,
                "title", "Labyrint sveta a raj srdce"
        ));
    }

    @Test
    void testIntegerFieldNullableFalseInsert() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Book romeo = BookFactory.createRomeo();
        Date birthDate = new GregorianCalendar(1996, Calendar.DECEMBER, 25).getTime();

        try {
            winterORM.insert(Friend.class, Map.of(
                    "name", "Stanislav",
                    "favoriteAuthor", shakespeare,
                    "favoriteBook", romeo,
                    "birthDate", birthDate
            ));

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testIntegerFieldNullableTrueSet() throws WinterORMException {
        Book labyrint = BookFactory.createLabyrinth();

        labyrint.set(new HashMap<>() {{
            put("publicationYear", null);
        }});
    }

    @Test
    void testIntegerFieldNullableFalseSet() throws WinterORMException {
        Friend stanislav = FriendFactory.createStanislav();

        try {
            stanislav.set(new HashMap<>() {{
                put("age", null);
            }});

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testStringFieldNullableTrueInsert() throws WinterORMException {
        winterORM.insert(Author.class, Map.of(
                "name", "Molier"
        ));
    }

    @Test
    void testStringFieldNullableFalseInsert() throws WinterORMException {
        try {
            winterORM.insert(Author.class, new HashMap<>() {{
                put("name", null);
                put("surname", "Shakespeare");
            }});

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testStringFieldNullableTrueSet() throws WinterORMException {
        Author shakespeare = AuthorFactory.createMolier();

        shakespeare.set(new HashMap<>() {{
            put("surname", null);
        }});
    }

    @Test
    void testStringFieldNullableFalseSet() throws WinterORMException {
        Author shakespeare = AuthorFactory.createMolier();

        try {
            shakespeare.set(new HashMap<>() {{
                put("name", null);
            }});

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testStringFieldMaxLengthLimitExceededInsert() throws WinterORMException {
        String name = "MolierMolierMolierMolierMolierMolierMolierMolierMolier";
        Assertions.assertTrue(name.length() > 32);

        try {
            winterORM.insert(Author.class, Map.of(
                    "name", name
            ));

            fail();
        } catch (FieldMaxLengthExceededException e) {
            // OK.
        }
    }

    @Test
    void testStringFieldMaxLengthLimitNotExceededInsert() throws WinterORMException {
        String name = "Molier";
        Assertions.assertTrue(name.length() <= 32);

        winterORM.insert(Author.class, Map.of(
                "name", name
        ));
    }

    @Test
    void testStringFieldMaxLengthUnlimitedInsert() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        String title = "RomeoAndJulietRomeoAndJulietRomeoAndJulietRomeoAndJulietRomeoAndJuliet";
        Assertions.assertTrue(title.length() > 32);

        winterORM.insert(Book.class, Map.of(
                "author", shakespeare,
                "title", title
        ));
    }

    @Test
    void testStringFieldMaxLengthLimitExceededSet() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        String newName = "MolierMolierMolierMolierMolierMolierMolierMolierMolier";
        Assertions.assertTrue(newName.length() > 32);

        try {
            molier.set(Map.of(
                    "name", newName
            ));

            fail();
        } catch (FieldMaxLengthExceededException e) {
            // OK.
        }
    }

    @Test
    void testStringFieldMaxLengthLimitNotExceededSet() throws WinterORMException {
        Author molier = AuthorFactory.createMolier();
        String newName = "MolierMolier";
        Assertions.assertTrue(newName.length() <= 32);

        molier.set(Map.of(
                "name", newName
        ));
    }

    @Test
    void testStringFieldMaxLengthUnlimitedSet() throws WinterORMException {
        Book romeo = BookFactory.createRomeo();
        String newTitle = "TitleTitleTitleTitleTitleTitleTitleTitleTitleTitleTitle";
        Assertions.assertTrue(newTitle.length() > 32);

        romeo.set(Map.of(
                "title", newTitle
        ));
    }

    @Test
    void testDateFieldNullableTrueInsert() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();

        winterORM.insert(Book.class, Map.of(
                "author", komensky,
                "title", "Labyrint sveta a raj srdce"
        ));
    }

    @Test
    void testDateFieldNullableFalseInsert() throws WinterORMException {
        Author shakespeare = AuthorFactory.createShakespeare();
        Book romeo = BookFactory.createRomeo();

        try {
            winterORM.insert(Friend.class, Map.of(
                    "name", "Stanislav",
                    "favoriteAuthor", shakespeare,
                    "favoriteBook", romeo,
                    "age", 24
            ));

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testDateFieldNullableTrueSet() throws WinterORMException {
        Book othello = BookFactory.createOthello();

        othello.set(new HashMap<>() {{
            put("publicationYear", null);
        }});
    }

    @Test
    void testDateFieldNullableFalseSet() throws WinterORMException {
        Friend stanislav = FriendFactory.createStanislav();

        try {
            stanislav.set(new HashMap<>() {{
                put("birthDate", null);
            }});

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testForeignFieldNullableTrueInsert() throws WinterORMException {
        Book romeo = BookFactory.createRomeo();
        Date birthDate = new GregorianCalendar(1996, Calendar.DECEMBER, 25).getTime();

        winterORM.insert(Friend.class, Map.of(
                "name", "Stanislav",
                "favoriteBook", romeo,
                "age", 24,
                "birthDate", birthDate
        ));
    }

    @Test
    void testForeignFieldNullableFalseInsert() throws WinterORMException {
        try {
            winterORM.insert(Book.class, Map.of(
                    "title", "Labyrint sveta a raj srdce"
            ));

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testForeignFieldNullableTrueSet() throws WinterORMException {
        Friend stanislav = FriendFactory.createStanislav();

        stanislav.set(new HashMap<>() {{
            put("favoriteAuthor", null);
        }});
    }

    @Test
    void testForeignFieldNullableFalseSet() throws WinterORMException {
        Book labyrinth = BookFactory.createLabyrinth();

        try {
            labyrinth.set(new HashMap<>() {{
                put("author", null);
            }});

            fail();
        } catch (FieldNotNullableException e) {
            // OK.
        }
    }

    @Test
    void testForeignFieldOnDeleteSetNull() throws WinterORMException {
        Date birthDate = new GregorianCalendar(1996, Calendar.DECEMBER, 25).getTime();
        Author shakespeare = AuthorFactory.createShakespeare();
        Book romeo = BookFactory.createRomeo();

        winterORM.insert(Friend.class, Map.of(
                "name", "Stanislav",
                "favoriteAuthor", shakespeare,
                "favoriteBook", romeo,
                "age", 24,
                "birthDate", birthDate
        ));

        int friendCount = winterORM.getAll(Friend.class).getList().size();
        Assertions.assertEquals(1, friendCount);

        shakespeare.delete();

        List<Friend> friends = winterORM.getAll(Friend.class).getList();
        Assertions.assertEquals(1, friends.size());

        Friend stanislav = friends.get(0);
        Assertions.assertNull(stanislav.get("author"));
    }

    @Test
    void testForeignFieldOnDeleteDelete() throws WinterORMException {
        Author komensky = AuthorFactory.createKomensky();

        winterORM.insert(Book.class, Map.of(
                "author", komensky,
                "title", "Labyrint sveta a raj srdce"
        ));

        int bookCountBefore = winterORM.getAll(Book.class).getList().size();
        Assertions.assertEquals(1, bookCountBefore);

        komensky.delete();

        int bookCountAfter = winterORM.getAll(Book.class).getList().size();
        Assertions.assertEquals(0, bookCountAfter);
    }
}
