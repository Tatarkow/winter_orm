package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.AuthorFactory;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.filters.IntegerFilter;
import cz.tatarko.tables.Author;
import cz.tatarko.tables.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class TestSet extends BaseTest {
    @Test
    void testSetOnResult() throws WinterORMException {
        BookFactory.createOthello();
        BookFactory.createTartuffe();
        String romeoID = BookFactory.createRomeo().getID();

        Author komensky = AuthorFactory.createKomensky();
        int year = 2000;

        List<Book> books = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.MORE_THAN, 1600)
                .set(Map.of(
                        "author", komensky,
                        "publicationYear", 2000
                ))
                .getList();

        for (Book book: books) {
            Assertions.assertEquals(komensky, book.get("author"));
            Assertions.assertEquals(year, book.get("publicationYear"));
        }

        List<Book> retrievedBooks = winterORM
                .getAll(Book.class)
                .filter("publicationYear", IntegerFilter.EQUALS, year)
                .getList();

        for (Book book: retrievedBooks) {
            Assertions.assertEquals(komensky, book.get("author"));
            Assertions.assertEquals(year, book.get("publicationYear"));
        }

        Book romeo = winterORM.getAll(Book.class).get(romeoID);
        Assertions.assertNotEquals(komensky, romeo.get("author"));
        Assertions.assertNotEquals(year, romeo.get("publicationYear"));
    }

    @Test
    void testSetOnTableObject() throws WinterORMException {
        String shakespeareID = AuthorFactory.createShakespeare().getID();
        Author komensky = AuthorFactory.createKomensky();

        String newName = "Honza";
        Date newBirthDate = new GregorianCalendar(1900, Calendar.JANUARY, 5).getTime();

        komensky.set(Map.of(
                "name", newName,
                "birthDate", newBirthDate
        ));

        Author retrivedKomensky = winterORM.getAll(Author.class).get(komensky.getID());
        Assertions.assertEquals(newName, retrivedKomensky.get("name"));
        Assertions.assertEquals(newBirthDate, retrivedKomensky.get("birthDate"));

        Author shakespeare = winterORM.getAll(Author.class).get(shakespeareID);
        Assertions.assertNotEquals(newName, shakespeare.get("name"));
        Assertions.assertNotEquals(newBirthDate, shakespeare.get("birthDate"));
    }
}
