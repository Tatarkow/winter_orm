package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.factories.BookFactory;
import cz.tatarko.factories.Factory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import java.text.Format;
import java.text.SimpleDateFormat;

public abstract class BaseTest {
    protected static WinterORM winterORM;
    protected static Format formatter = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeAll
    static void initialize() throws WinterORMException {
        connect();
        createTables();
        initializeFactories();
    }

    static void connect() {
        try {
            DatabaseHandler databaseHandler = DatabaseHandlerHelper.getDatabaseHandler();

            if (databaseHandler.getClass() == PostgreSQLHandler.class) {
                connectToPostgreSQL();
            }
            else if (databaseHandler.getClass() == MongoDBHandler.class) {
                connectToMongoDB();
            }
            else if (databaseHandler.getClass() == H2Handler.class) {
                connectToH2();
            }

        } catch (WinterORMException e) {
            e.printStackTrace();
        }
    }

    private static void connectToPostgreSQL() throws WinterORMException {
        String name = "winter_orm_database";
        String user = "winter_orm_user";
        String password = "winter_orm_password";
        String host = "localhost";
        String port = "5432";

        winterORM = new WinterORM("cz.tatarko.tables", name, user, password, host, port);
    }

    private static void connectToMongoDB() throws WinterORMException {
        String name = "winter_orm_database";
        String user = "winter_orm_user";
        String password = "winter_orm_password";
        String host = "localhost";
        String port = "27017";

        winterORM = new WinterORM("cz.tatarko.tables", name, user, password, host, port);
    }

    private static void connectToH2() throws WinterORMException {
        String name = "winter_orm_database";
        String user = null;
        String password = null;
        String host = null;
        String port = null;

        winterORM = new WinterORM("cz.tatarko.tables", name, user, password, host, port);
    }

    private static void createTables() throws WinterORMException {
        winterORM.createTables();
    }

    private static void initializeFactories() {
        Factory.setWinterORM(winterORM);
        BookFactory.setWinterORM(winterORM);
    }

    @AfterEach
    void clearDatabase() throws WinterORMException {
        winterORM.clearDatabase();
    }

    @AfterAll
    static void finish() throws WinterORMException {
        winterORM.finish();
    }
}
