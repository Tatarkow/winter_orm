package cz.tatarko;

import cz.tatarko.fields.ForeignField;
import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.exceptions.table.TableAlreadyExistsException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

import static cz.tatarko.TableHelper.isTableClass;

class TableCreator {
    private static DatabaseHandler databaseHandler;

    protected static void createTables() throws WinterORMException {
        Set<Class<TableObject>> tableClasses = TableHelper.getTableClasses();
        List<Class<TableObject>> orderedTableClasses = orderTableClasses(tableClasses);
        Set<String> existingTableNames = databaseHandler.getExistingTableNames();

        for (Class<TableObject> tableClass: orderedTableClasses) {
            try {
                createTable(tableClass, existingTableNames);
            }
            catch (TableAlreadyExistsException e) {
                String tableName = TableHelper.getTableName(tableClass);
                System.err.printf("Table %s already exists. Creating table skipped.%n", tableName);
            }
        }
    }

    private static void createTable(Class<TableObject> tableClass, Set<String> existingTableNames) throws WinterORMException {
        String tableName = TableHelper.getTableName(tableClass);

        if (existingTableNames.contains(tableName)) {
            throw new TableAlreadyExistsException();
        }

        Field[] fields = tableClass.getDeclaredFields();
        databaseHandler.createTable(tableName, fields);
    }

    /**
     * Orders table classes based on the dependencies. E.g. if A depends on B, then B will be first.
     */
    private static List<Class<TableObject>> orderTableClasses(Set<Class<TableObject>> tableClasses) {
        List<DependencyInfo> dependencyInfos = getDependencyInfo(tableClasses);
        List<Class<TableObject>> orderTableClasses = new ArrayList<>();

        while (dependencyInfos.size() > 0) {
            dependencyInfos.sort(Comparator.comparingInt((DependencyInfo d) -> -d.dependencies.size()));
            Set<Class<TableObject>> withoutDependencies = new HashSet<>();
            DependencyInfo last = dependencyInfos.get(dependencyInfos.size()-1);

            while (last.dependencies.size() == 0) {
                withoutDependencies.add(last.tableClass);
                dependencyInfos.remove(last);

                if (dependencyInfos.size() == 0) {
                    break;
                }

                last = dependencyInfos.get(dependencyInfos.size()-1);
            }

            for (DependencyInfo dependencyInfo: dependencyInfos) {
                dependencyInfo.dependencies.removeAll(withoutDependencies);
            }

            orderTableClasses.addAll(withoutDependencies);
        }

        return orderTableClasses;
    }

    private static List<DependencyInfo> getDependencyInfo(Set<Class<TableObject>> tableClasses) {
        List<DependencyInfo> dependencyInfos = new ArrayList<>();

        for (Class<TableObject> tableClass: tableClasses) {
            DependencyInfo dependencyInfo = new DependencyInfo();
            dependencyInfo.tableClass = tableClass;
            dependencyInfo.dependencies = getDependencies(tableClass);
            dependencyInfos.add(dependencyInfo);
        }

        return dependencyInfos;
    }

    private static Set<Class<TableObject>> getDependencies(Class<TableObject> tableClass) {
        Set<Class<TableObject>> dependencies = new HashSet<>();
        Field[] fields = tableClass.getDeclaredFields();

        for (Field field: fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();

            for (Annotation annotation: annotations) {
                if (annotation.annotationType().equals(ForeignField.class)) {
                    Class<?> fieldType = field.getType();

                    if (isTableClass(fieldType)) {
                        dependencies.add((Class<TableObject>) fieldType);
                    }
                }
            }
        }

        return dependencies;
    }

    static void setDatabaseHandler(DatabaseHandler databaseHandler) {
        TableCreator.databaseHandler = databaseHandler;
    }
}
