package cz.tatarko;

import cz.tatarko.exceptions.ReflectionException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

class Utils {
    static boolean containsLettersOnly(String tableName) {
        return Pattern.matches("[a-zA-Z]+", tableName);
    }

    public static <T extends TableObject> Field getDeclaredField(Class<T> tableClass, String columnName) throws ReflectionException {
        try {
            return tableClass.getDeclaredField(columnName);
        } catch (NoSuchFieldException e) {
            throw new ReflectionException();
        }
    }

    public static <T extends TableObject> void setFieldValue(Field field, T object, Object value) throws ReflectionException {
        try {
            field.setAccessible(true);
            field.set(object, value);
        } catch (IllegalAccessException e) {
            throw new ReflectionException();
        }
    }

    public static <T extends TableObject> Object getFieldValue(Field field, T tableObject) throws ReflectionException {
        try {
            field.setAccessible(true);
            return field.get(tableObject);
        } catch (IllegalAccessException e) {
            throw new ReflectionException();
        }
    }

    public static <T extends TableObject> T getInstance(Class<T> tableClass) throws ReflectionException {
        try {
            return tableClass.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new ReflectionException();
        }
    }
}
