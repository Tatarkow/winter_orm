package cz.tatarko;

import cz.tatarko.exceptions.ORMSQLException;
import org.h2.jdbc.JdbcClob;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;


/**
 * Communicates with H2 database.
 */
public class H2Handler extends SQLHandler {
    @Override
    String getURL(String name, String host, String port) {
        return String.format("jdbc:h2:./%s", name);
    }

    @Override
    String getQueryForExistingTableNames() {
        return "SHOW TABLES;";
    }

    @Override
    public void clearDatabase() throws ORMSQLException {
        Set<String> tableNames = getExistingTableNames();
        StringBuilder queryUpdate = new StringBuilder();

        queryUpdate.append("SET REFERENTIAL_INTEGRITY FALSE;");
        queryUpdate.append("BEGIN TRANSACTION;");

        for (String tableName: tableNames) {
            queryUpdate.append(String.format("DELETE FROM %s; ", tableName));
        }

        queryUpdate.append("COMMIT;");
        queryUpdate.append("SET REFERENTIAL_INTEGRITY TRUE;");

        executeUpdate(queryUpdate);
    }

    @Override
    protected Object getObjectFromResultSet(ResultSet resultSet, String identification) throws SQLException {
        Object value = resultSet.getObject(identification);

        if (value == null) {
            return null;
        }
        else if (value.getClass() == JdbcClob.class) {
            return resultSet.getString(identification);
        }
        else {
            return value;
        }
    }
}
