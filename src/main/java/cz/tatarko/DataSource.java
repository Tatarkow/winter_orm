package cz.tatarko;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import cz.tatarko.exceptions.ConnectionException;

import java.sql.Connection;
import java.sql.SQLException;

class DataSource {
    static Connection getConnection(String url, String user, String password) throws ConnectionException {
        HikariConfig config = getConfig(url, user, password);
        HikariDataSource hikariDataSource;

        try {
            hikariDataSource = new HikariDataSource(config);
        } catch(Exception e) {
            throw new ConnectionException("Check that the database is running.");
        }

        try {
            return hikariDataSource.getConnection();
        } catch (SQLException e) {
            String message = String.format("Error occurred when connection to %s with user %s", url, user);
            throw new ConnectionException(message);
        }
    }

    private static HikariConfig getConfig(String url, String user, String password) {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(url);
        config.setUsername(user);
        config.setPassword(password);

        return config;
    }
}