package cz.tatarko;

import cz.tatarko.exceptions.UnknownFilterException;
import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.filters.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

class SQLFilterHelper {
    static <T extends TableObject> String filterToQuery(Class<T> tableClass, Filter filter) throws WinterORMException {
        String tableName = TableHelper.getTableName(tableClass);
        String columnName = filter.getColumnName();
        FilterType filterType = filter.getFilterType();
        String identification = String.format("%s.%s", tableName, columnName);
        List<Object> values = filter.getValues();

        if (filterType == ForeignFilter.COMPARE) {
            return getQueryForeignCompare(tableClass, columnName, values);
        }
        else {
            return getQuery(identification, filterType, values);
        }
    }

    private static String getQuery(String identification, FilterType filterType, List<Object> values) throws UnknownFilterException {
        if (filterType == StringFilter.EQUALS) {
            return getQueryEquals(identification);
        }
        else if (filterType == StringFilter.IS_EMPTY) {
            return getQueryIsEmpty(values, identification);
        }
        else if (filterType == IntegerFilter.EQUALS) {
            return getQueryEquals(identification);
        }
        else if (filterType == IntegerFilter.LESS_THAN) {
            return getQueryLessThan(identification);
        }
        else if (filterType == IntegerFilter.MORE_THAN) {
            return getQueryMoreThan(identification);
        }
        else if (filterType == IntegerFilter.IS_NULL) {
            return getQueryIsNull(values, identification);
        }
        else if (filterType == DateFilter.EQUALS) {
            return getQueryEquals(identification);
        }
        else if (filterType == DateFilter.IS_NULL) {
            return getQueryIsNull(values, identification);
        }
        else if (filterType == ForeignFilter.EQUALS) {
            return getQueryEquals(identification);
        }
        else if (filterType == IDFilter.EQUALS) {
            return getQueryEquals(identification);
        }
        else {
            throw new UnknownFilterException();
        }
    }

    private static String getQueryIsNull(List<Object> values, String identification) {
        if (values.size() == 0) {
            return String.format("%s IS NULL ", identification);
        }
        else {
            Boolean isEmpty = (Boolean) values.get(0);

            if (isEmpty) {
                return String.format("%s IS NULL", identification);
            }
            else {
                return String.format("%s IS NOT NULL", identification);
            }
        }
    }

    private static <T extends TableObject> String getQueryForeignCompare(Class<T> tableClass, String columnName, List<Object> values) throws WinterORMException {
        Field field = Utils.getDeclaredField(tableClass, columnName);
        String relatedTableName = TableHelper.getTableName(field.getType());
        List<Object> copiedValues = new ArrayList<>(values);
        String relatedFieldName = (String) copiedValues.remove(0);
        FilterType filterType = (FilterType) copiedValues.remove(0);
        String identification;

        if ("id".equals(columnName)) {
            String tableName = TableHelper.getTableName(tableClass);
            identification = String.format("%s.%s", tableName, relatedTableName);
        }
        else {
            if ("id".equals(relatedFieldName)) {
                identification = String.format("%s", columnName);
            }
            else {
                identification = String.format("%s.%s", relatedTableName, relatedFieldName);
            }
        }

        return getQuery(identification, filterType, copiedValues);
    }

    private static String getQueryEquals(String identification) {
        return String.format("%s = ? ", identification);
    }

    private static String getQueryLessThan(String identification) {
        return String.format("%s < ? ", identification);
    }

    private static String getQueryMoreThan(String identification) {
        return String.format("%s > ? ", identification);
    }

    private static String getQueryIsEmpty(List<Object> values, String identification) {
        Boolean isEmpty;

        if (values.size() == 0) {
            isEmpty = true;
        }
        else {
            isEmpty = (Boolean) values.get(0);
        }

        if (isEmpty) {
            return String.format("(%s IS NULL OR %s = '')", identification, identification);
        }
        else {
            return String.format("(%s IS NOT NULL AND %s != '')", identification, identification);
        }
    }
}
