package cz.tatarko;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.tatarko.exceptions.ConnectionException;
import cz.tatarko.exceptions.ORMSQLException;
import cz.tatarko.exceptions.ObtainingExistingTableNamesException;
import cz.tatarko.exceptions.WinterORMException;

/**
 * Interface for database handlers, such as MongoDBHandler that communicates with MongoDB database.
 */
interface DatabaseHandler {
    /**
     * Connect to the database and initializes an object through which it will be possible to communicate with the database.
     */
    void connect(String name, String user, String password, String host, String port) throws ConnectionException;

    /**
     * Returns a set of names of the all tables in the database.
     */
    Set<String> getExistingTableNames() throws ObtainingExistingTableNamesException;

    /**
     * Creates a database table based on the defined table class.
     */
    void createTable(String tableName, Field[] fields) throws WinterORMException;

    /**
     * Deletes all the data from the database. Keeps the database structure.
     */
    void clearDatabase() throws ORMSQLException;

    /**
     * Inserts the given table object into a database table with the given name.
     */
    <T extends TableObject> String insert(String tableName, T tableObject) throws WinterORMException;

    /**
     * Returns a list of table objects based of the previous database interaction (e.g. filters).
     */
    <T extends TableObject> List<T> select(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException;

    /**
     * Deletes all the table objects satisfying the previously provided filters.
     */
    <T extends TableObject> void delete(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException;

    /**
     * Updates all the table objects satisfying the previously provided filters based on the provided values.
     */
    <T extends TableObject> void set(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters, Map<String, Object> values) throws WinterORMException;

    /**
     * This should be called when a user will not be communicating with the database anymore. Namely it closes the connection.
     */
    void finish() throws ORMSQLException;
}
