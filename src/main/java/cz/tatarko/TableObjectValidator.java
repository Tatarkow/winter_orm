package cz.tatarko;

import cz.tatarko.fields.DateField;
import cz.tatarko.fields.ForeignField;
import cz.tatarko.fields.IntegerField;
import cz.tatarko.fields.StringField;
import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.exceptions.table.AnnotationCountException;
import cz.tatarko.exceptions.table.TableException;
import cz.tatarko.exceptions.table.InvalidAnnotationException;
import cz.tatarko.exceptions.table.InvalidDatabaseNameException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

class TableObjectValidator {
    static void validate() throws WinterORMException {
        Set<Class<TableObject>> tableClasses = TableHelper.getTableClasses();

        for (Class<TableObject> tableClass: tableClasses) {
            validateClass(tableClass);
        }
    }

    private static void validateClass(Class<TableObject> tableClass) throws TableException {
        String tableName = TableHelper.getTableName(tableClass);

        if (!Utils.containsLettersOnly(tableName)) {
            throw new InvalidDatabaseNameException("Table cannot has this name.");
        }

        Field[] fields = tableClass.getDeclaredFields();

        for (Field field: fields) {
            validateField(field);
        }
    }

    private static void validateField(Field field) throws TableException {
        String fieldName = field.getName();

        if (!Utils.containsLettersOnly(fieldName)) {
            throw new InvalidDatabaseNameException("Field cannot has this name.");
        }

        Annotation[] annotations = field.getDeclaredAnnotations();

        if (annotations.length != 1) {
            throw new AnnotationCountException();
        }

        Annotation annotation = annotations[0];
        Class<? extends Annotation> annotationType = annotation.annotationType();

        if (!isAnnotationTypeValid(field, annotationType)) {
            throw new InvalidAnnotationException("Annotation of this type cannot be use on a field fo this type.");
        }
    }

    private static boolean isAnnotationTypeValid(Field field, Class<? extends Annotation> annotationType) {
        Class<?> fieldType = field.getType();
        Set<Class<? extends Annotation>> allowedAnnotations = new HashSet<>();

        if (fieldType == String.class) {
            allowedAnnotations.add(StringField.class);
        }
        else if (fieldType == Integer.class) {
            allowedAnnotations.add(IntegerField.class);
        }
        else if (fieldType == Date.class) {
            allowedAnnotations.add(DateField.class);
        }
        else if (TableHelper.isTableClass(fieldType)) {
            allowedAnnotations.add(ForeignField.class);
        }

        return allowedAnnotations.contains(annotationType);
    }
}
