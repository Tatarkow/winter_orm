package cz.tatarko.filters;

/**
 * Contains filters of filtering results based on a foreign object.
 */
public enum ForeignFilter implements FilterType {
    EQUALS, COMPARE
}
