package cz.tatarko.filters;

/**
 * Contains filters of filtering results based on ID.
 */
public enum IDFilter implements FilterType {
    EQUALS
}
