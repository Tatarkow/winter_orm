package cz.tatarko.filters;

/**
 * Contains filters of filtering results based on string.
 */
public enum StringFilter implements FilterType {
    EQUALS, IS_EMPTY
}
