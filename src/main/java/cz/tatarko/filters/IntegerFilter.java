package cz.tatarko.filters;

/**
 * Contains filters of filtering results based on integer.
 */
public enum IntegerFilter implements FilterType {
    EQUALS, LESS_THAN, MORE_THAN, IS_NULL
}
