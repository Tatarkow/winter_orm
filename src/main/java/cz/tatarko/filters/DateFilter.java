package cz.tatarko.filters;

/**
 * Contains filters of filtering results based on date.
 */
public enum DateFilter implements FilterType {
    EQUALS, IS_NULL
}
