package cz.tatarko.fields;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation may be used on fields of type that inherits from cz.tatarko.TableObject.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ForeignField {
    boolean nullable() default true;
    OnDelete onDelete() default OnDelete.SET_NULL;
}
