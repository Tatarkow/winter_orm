package cz.tatarko.fields;

/**
 * Specifies what to do when a related object is deleted.
 */
public enum OnDelete {
    DELETE, SET_NULL
}
