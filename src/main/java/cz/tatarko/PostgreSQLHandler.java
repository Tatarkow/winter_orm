package cz.tatarko;

import cz.tatarko.exceptions.ORMSQLException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 * Communicates with PostgreSQL database.
 */
public class PostgreSQLHandler extends SQLHandler {
    @Override
    String getURL(String name, String host, String port) {
        return String.format("jdbc:postgresql://%s:%s/%s", host, port, name);
    }

    @Override
    protected Object getObjectFromResultSet(ResultSet resultSet, String identification) throws SQLException {
        return resultSet.getObject(identification);
    }

    @Override
    public void clearDatabase() throws ORMSQLException {
        Set<String> tableNames = getExistingTableNames();
        StringBuilder queryUpdate = new StringBuilder();

        for (String tableName: tableNames) {
            queryUpdate.append(String.format("TRUNCATE %s CASCADE;", tableName));
        }

        executeUpdate(queryUpdate);
    }

    @Override
    String getQueryForExistingTableNames() {
        return (
            "SELECT table_name " +
            "FROM information_schema.tables " +
            "WHERE table_schema='public' " +
            "AND table_type='BASE TABLE';"
        );
    }
}
