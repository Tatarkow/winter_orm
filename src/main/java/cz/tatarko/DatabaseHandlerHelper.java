package cz.tatarko;

import java.util.Iterator;
import java.util.ServiceLoader;

import cz.tatarko.exceptions.MissingDatabaseHandlerException;

class DatabaseHandlerHelper {
    static DatabaseHandler getDatabaseHandler() throws MissingDatabaseHandlerException {
        ServiceLoader<DatabaseHandler> serviceLoader = ServiceLoader.load(DatabaseHandler.class);
        Iterator<DatabaseHandler> serviceIterator = serviceLoader.iterator();

        if (serviceIterator.hasNext()) {
            return serviceIterator.next();
        }
        else {
            throw new MissingDatabaseHandlerException();
        }
    }
}
