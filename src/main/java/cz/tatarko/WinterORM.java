package cz.tatarko;

import java.lang.reflect.Field;
import java.util.*;

import cz.tatarko.exceptions.*;
import cz.tatarko.exceptions.table.NotTableClassException;

import static cz.tatarko.TableHelper.*;

/**
 * The main class for database communication. It is meant to be instantiated only once.
 */
public class WinterORM {
    private DatabaseHandler databaseHandler;

    /**
     * Performs initialization of the ORM.
     */
    public WinterORM(String packageName, String name, String user, String password, String host, String port) throws WinterORMException {
        prepareTableHelper(packageName);
        validateTableObjects();
        setDatabaseHandler();
        connectToDatabase(name, user, password, host, port);
        prepareTableCreator();
        prepareResult();
        prepareTableObject();
    }

    private void prepareTableHelper(String packageName) {
        TableHelper.setPackageName(packageName);
    }

    private void validateTableObjects() throws WinterORMException {
        TableObjectValidator.validate();
    }

    private void setDatabaseHandler() throws MissingDatabaseHandlerException {
        databaseHandler = DatabaseHandlerHelper.getDatabaseHandler();
    }

    private void connectToDatabase(String name, String user, String password, String host, String port) throws ConnectionException {
        databaseHandler.connect(name, user, password, host, port);
    }

    private void prepareTableCreator() {
        TableCreator.setDatabaseHandler(databaseHandler);
    }

    private void prepareResult() {
        Result.setWinterORM(this);
        Result.setDatabaseHandler(databaseHandler);
    }

    private void prepareTableObject() {
        TableObject.setWinterORM(this);
    }

    /**
     * Creates database tables based on the defined table classes.
     */
    public void createTables() throws WinterORMException {
        TableCreator.createTables();
    }

    /**
     * Deletes all the data from the database. Keeps the database structure.
     */
    public void clearDatabase() throws ORMSQLException {
        databaseHandler.clearDatabase();
    }

    /**
     * Inserts an object of the provided type with the provided values to the database.
     */
    public <T extends TableObject> T insert(Class<T> tableClass, Map<String, Object> values) throws WinterORMException {
        if (!isTableClass(tableClass)) {
            throw new NotTableClassException("Only table classes can be inserted.");
        }

        Field[] fields = tableClass.getDeclaredFields();
        Validator.validateInsert(fields, values);

        T tableObject = TableObjectHelper.getInstance(tableClass);
        TableObjectHelper.setValues(tableObject, values);

        String tableName = TableHelper.getTableName(tableClass);
        String id = databaseHandler.insert(tableName, tableObject);
        tableObject.setID(id);

        return tableObject;
    }

    /**
     * Provides access to database data of the given type.
     */
    public <T extends TableObject> Result<T> getAll(Class<T> tableClass) {
        return new Result<>(tableClass);
    }

    public void finish() throws WinterORMException {
        databaseHandler.finish();
    }
}
