package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.exceptions.table.MissingTablesException;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

class TableHelper {
    private static String packageName;

    static void setPackageName(String packageName) {
        TableHelper.packageName = packageName;
    }

    static String getTableName(Class<?> tableClass) {
        String[] classNameParts = tableClass.getName().split("\\.");
        String name = classNameParts[classNameParts.length - 1];

        return name.toLowerCase();
    }

    static boolean isTableClass(Class<?> cls) {
        return TableObject.class.isAssignableFrom(cls);
    }

    static Annotation getAnnotation(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();

        // This is safe due to TableObjectValidator.
        return annotations[0];
    }

    static Class<? extends Annotation> getAnnotationClass(Field field) {
        Annotation annotation = getAnnotation(field);
        return annotation.annotationType();
    }

    static Set<Class<TableObject>> getTableClasses() throws WinterORMException {
        return getClasses()
            .stream()
            .filter(TableHelper::isTableClass)
            .map(cls -> (Class<TableObject>) cls)
            .collect(Collectors.toSet());
    }

    private static Set<Class<?>> getClasses() throws WinterORMException {
        File folder = getFolder(packageName);
        File[] files = folder.listFiles();

        if (files == null) {
            throw new MissingTablesException();
        }

        Set<Class<?>> classes = new HashSet<>();

        for (File file: files) {
            if (representsClass(file)) {
                String fileName = file.getName();
                String fileNameOnly = removeClassExtension(fileName);
                String fullClass = packageName + '.' + fileNameOnly;

                try {
                    classes.add(Class.forName(fullClass));
                } catch (ClassNotFoundException e) {
                    throw new MissingTablesException();
                }
            }
        }

        return classes;
    }

    private static File getFolder(String packageName) throws MissingTablesException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        String path = packageName.replace('.', '/');
        URL resource = classLoader.getResource(path);

        if (resource == null) {
            throw new MissingTablesException();
        }

        return new File(resource.getFile());
    }

    private static boolean representsClass(File file) {
        return file.isFile() && file.getName().endsWith(".class");
    }

    private static String removeClassExtension(String fileName) {
        return fileName.substring(0, fileName.length() - 6);
    }
}
