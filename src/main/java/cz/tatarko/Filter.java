package cz.tatarko;

import cz.tatarko.filters.FilterType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Filter {
    private String columnName;
    private FilterType filterType;
    private List<Object> values;

   Filter(String columnName, FilterType filterType, Object... values) {
       this.columnName = columnName;
       this.filterType = filterType;
       this.values = new ArrayList<>(Arrays.asList(values));
    }

    public FilterType getFilterType() {
        return filterType;
    }

    public String getColumnName() {
        return columnName;
    }

    public List<Object> getValues() {
        return values;
    }
}
