package cz.tatarko;

import cz.tatarko.fields.DateField;
import cz.tatarko.fields.ForeignField;
import cz.tatarko.fields.IntegerField;
import cz.tatarko.fields.StringField;
import cz.tatarko.exceptions.validation.FieldMaxLengthExceededException;
import cz.tatarko.exceptions.validation.FieldNotNullableException;
import cz.tatarko.exceptions.WinterORMException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

class Validator {
    static void validateSet(Field[] fields, Map<String, Object> values) throws WinterORMException {
        for (Field field: fields) {
            String fieldName = field.getName();

            if (!values.containsKey(fieldName)) {
                continue;
            }

            Object value = values.get(fieldName);
            validateField(field, value);
        }
    }

    static void validateInsert(Field[] fields, Map<String, Object> values) throws WinterORMException {
        for (Field field: fields) {
            String fieldName = field.getName();
            Object value = null;

            if (values.containsKey(fieldName)) {
                value = values.get(fieldName);
            }

            validateField(field, value);
        }
    }

    private static void validateField(Field field, Object value) throws WinterORMException {
        Annotation generalAnnotation = TableHelper.getAnnotation(field);
        Class<? extends Annotation> annotationClass = generalAnnotation.annotationType();

        if (annotationClass == IntegerField.class) {
            validateIntegerField(generalAnnotation, value);
        }
        else if (annotationClass == StringField.class) {
            validateStringField(generalAnnotation, value);
        }
        else if (annotationClass == DateField.class) {
            validateDateField(generalAnnotation, value);
        }
        else if (annotationClass == ForeignField.class) {
            validateForeignField(generalAnnotation, value);
        }
    }

    private static void validateForeignField(Annotation generalAnnotation, Object value) throws FieldNotNullableException {
        ForeignField annotation = (ForeignField) generalAnnotation;

        if (!annotation.nullable()) {
            checkNull(value);
        }
    }

    private static void validateDateField(Annotation generalAnnotation, Object value) throws FieldNotNullableException {
        DateField annotation = (DateField) generalAnnotation;

        if (!annotation.nullable()) {
            checkNull(value);
        }
    }

    private static void validateStringField(Annotation generalAnnotation, Object value) throws FieldNotNullableException, FieldMaxLengthExceededException {
        StringField annotation = (StringField) generalAnnotation;

        if (!annotation.nullable()) {
            checkNull(value);
        }

        if (annotation.maxLength() != 0) {
            if (value != null && ((String) value).length() > annotation.maxLength()) {
                throw new FieldMaxLengthExceededException();
            }
        }
    }

    private static void validateIntegerField(Annotation generalAnnotation, Object value) throws FieldNotNullableException {
        IntegerField annotation = (IntegerField) generalAnnotation;

        if (!annotation.nullable()) {
            checkNull(value);
        }
    }

    private static void checkNull(Object value) throws FieldNotNullableException {
        if (value == null) {
            throw new FieldNotNullableException();
        }
    }
}
