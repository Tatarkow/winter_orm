package cz.tatarko;

import cz.tatarko.filters.ForeignFilter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class RelatedFieldsAugmentor {
    static void augment(Set<String> relatedFieldNames, List<Filter> filters) {
        List<Filter> foreignFilters = filters
                .stream()
                .filter(filter -> filter.getFilterType() == ForeignFilter.COMPARE)
                .collect(Collectors.toList());

        Set<String> newRelatedFieldNames = foreignFilters
                .stream()
                .map(Filter::getColumnName)
                .collect(Collectors.toSet());

        relatedFieldNames.addAll(newRelatedFieldNames);
    }
}
