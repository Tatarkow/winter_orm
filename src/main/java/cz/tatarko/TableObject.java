package cz.tatarko;

import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.filters.IDFilter;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a single table object, i.e a row in some table of the database.
 */
public class TableObject {
    private static WinterORM winterORM;
    private boolean loaded = true;
    private String id;

    static void setWinterORM(WinterORM winterORM) {
        TableObject.winterORM = winterORM;
    }

    void setID(String id) {
        this.id = id;
    }

    /**
     * Retrieves the database ID of the table object.
     */
    public String getID() {
        return id;
    }

    /**
     * Retrieves a value of the field/column of the table object.
     */
    public Object get(String fieldName) throws WinterORMException {
        Class<? extends TableObject> tableClass = this.getClass();
        Field[] fields = tableClass.getDeclaredFields();

        for (Field field: fields) {
            if (fieldName.equals(field.getName())) {
                Object value = Utils.getFieldValue(field, this);

                if (TableHelper.isTableClass(field.getType())) {
                    TableObject foreignObject = (TableObject) value;
                    if (!foreignObject.loaded) {
                        String id = foreignObject.getID();
                        value = winterORM.getAll(foreignObject.getClass()).get(id);
                        foreignObject.loaded = true;
                    }
                }

                return value;
            }
        }

        return null;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        Class<? extends TableObject> thisClass = this.getClass();
        Class<?> otherClass = object.getClass();

        if (thisClass != otherClass) {
            return false;
        }

        Map<String, Object> otherFields = new HashMap<>();

        for (Field field: otherClass.getDeclaredFields()) {
            try {
                String name = field.getName();
                field.setAccessible(true);
                Object value = field.get(object);
                otherFields.put(name, value);
            } catch (IllegalAccessException e) {
                return false;
            }
        }

        for (Field thisField: thisClass.getDeclaredFields()) {
            String name = thisField.getName();

            if (otherFields.containsKey(name)) {
                try {
                    thisField.setAccessible(true);
                    Object thisValue = thisField.get(this);
                    Object otherValue = otherFields.get(name);

                    if (thisValue == null) {
                        if (otherValue != null) {
                            return false;
                        }
                    }
                    else {
                        if (!thisValue.equals(otherValue)) {
                            return false;
                        }
                    }
                } catch (IllegalAccessException e) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Deletes the table object.
     */
    public void delete() throws WinterORMException {
        winterORM
            .getAll(this.getClass())
            .filter("id", IDFilter.EQUALS, id)
            .delete();
    }

    /**
     * Updates the table object in the database with respect to the provided values.
     */
    public TableObject set(Map<String, Object> values) throws WinterORMException {
        winterORM
            .getAll(this.getClass())
            .filter("id", IDFilter.EQUALS, id)
            .set(values);

        return this;
    }

    /**
     * Updates the table object loaded in the memory with respect to the database.
     */
    public void refresh() throws WinterORMException {
        TableObject newTableObject = winterORM.getAll(this.getClass()).get(id);
        Field[] fields = getClass().getDeclaredFields();

        for (Field field: fields) {
            String fieldName = field.getName();
            Object value = newTableObject.get(fieldName);
            Utils.setFieldValue(field, this, value);
        }
    }

    /**
     * Returns whether the table object has been actually loaded, or whether the table objects holds basically only the ID.
     */
    public boolean getLoaded() {
        return loaded;
    }

    void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
}
