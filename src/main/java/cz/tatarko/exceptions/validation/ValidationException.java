package cz.tatarko.exceptions.validation;

import cz.tatarko.exceptions.WinterORMException;

/**
 * General exception for validation related problems.
 */
public abstract class ValidationException extends WinterORMException {
    static String defaultMessage = "Validation related problem occurred.";

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException() {
        super(defaultMessage);
    }
}
