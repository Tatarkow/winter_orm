package cz.tatarko.exceptions.validation;

/**
 * Exception indicating that maximal length of a field has been exceeded.
 */
public class FieldMaxLengthExceededException extends ValidationException {
    static String defaultMessage = "Maximal length of a field has been exceeded.";

    public FieldMaxLengthExceededException(String message) {
        super(message);
    }

    public FieldMaxLengthExceededException() {
        super(defaultMessage);
    }
}