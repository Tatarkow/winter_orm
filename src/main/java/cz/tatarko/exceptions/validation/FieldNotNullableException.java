package cz.tatarko.exceptions.validation;

/**
 * This exception is risen when a user tries to assign null to a not nullable field.
 */
public class FieldNotNullableException extends ValidationException {
    static String defaultMessage = "Trying to assign null to a not nullable field.";

    public FieldNotNullableException(String message) {
        super(message);
    }

    public FieldNotNullableException() {
        super(defaultMessage);
    }
}
