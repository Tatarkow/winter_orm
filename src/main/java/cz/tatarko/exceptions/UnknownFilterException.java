package cz.tatarko.exceptions;

/**
 * This exception used when there appears a filter of an unknown type.
 */
public class UnknownFilterException extends WinterORMException {
    static String defaultMessage = "Filter of this type is unknown.";

    public UnknownFilterException(String message) {
        super(message);
    }

    public UnknownFilterException() {
        super(defaultMessage);
    }
}
