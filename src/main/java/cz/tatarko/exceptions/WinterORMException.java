package cz.tatarko.exceptions;

/**
 * General exception of this application, from which all the other exceptions inherit.
 */
public abstract class WinterORMException extends Exception {
    public WinterORMException(String message) {
        super(message);
    }
}
