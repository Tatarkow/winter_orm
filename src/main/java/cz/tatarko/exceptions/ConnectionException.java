package cz.tatarko.exceptions;

/**
 * Exception indicating that there was a problem ith database connection.
 */
public class ConnectionException extends WinterORMException {
    static String defaultMessage = "Connection exception";

    public ConnectionException(String message) {
        super(message);
    }

    public ConnectionException() {
        super(defaultMessage);
    }
}
