package cz.tatarko.exceptions;

/**
 * This is exception is thrown when the database handler is not specified.
 */
public class MissingDatabaseHandlerException extends WinterORMException {
    static String defaultMessage = "Missing database handler.";

    public MissingDatabaseHandlerException(String message) {
        super(message);
    }

    public MissingDatabaseHandlerException() {
        super(defaultMessage);
    }
}
