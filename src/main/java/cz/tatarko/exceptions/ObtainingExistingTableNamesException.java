package cz.tatarko.exceptions;

/**
 * Exception indicating that names of existing database tables could not been obtained.
 */
public class ObtainingExistingTableNamesException extends ORMSQLException {
    static String defaultMessage = "Names of existing database tables could not been obtained.";

    public ObtainingExistingTableNamesException(String message) {
        super(message);
    }

    public ObtainingExistingTableNamesException() {
        super(defaultMessage);
    }
}
