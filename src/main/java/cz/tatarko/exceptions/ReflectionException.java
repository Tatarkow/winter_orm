package cz.tatarko.exceptions;

/**
 * Exception indicating that a problem with Java reflection occurred.
 */
public class ReflectionException extends WinterORMException {
    static String defaultMessage = "P";

    public ReflectionException(String message) {
        super(message);
    }

    public ReflectionException() {
        super(defaultMessage);
    }
}
