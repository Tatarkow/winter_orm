package cz.tatarko.exceptions;

/**
 * Exception indicating that a problem with SQL query occurred.
 */
public class ORMSQLException extends WinterORMException {
    static String defaultMessage = "Problem with SQL query occurred.";

    public ORMSQLException(String message) {
        super(message);
    }

    public ORMSQLException() {
        super(defaultMessage);
    }
}
