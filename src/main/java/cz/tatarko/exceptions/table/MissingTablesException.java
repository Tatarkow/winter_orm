package cz.tatarko.exceptions.table;

import cz.tatarko.exceptions.WinterORMException;

/**
 * Exception indicating that there is not definition of database tables.
 */
public class MissingTablesException extends WinterORMException {
    static String defaultMessage = "Missing definition of tables.";

    public MissingTablesException(String message) {
        super(message);
    }

    public MissingTablesException() {
        super(defaultMessage);
    }
}
