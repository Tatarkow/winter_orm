package cz.tatarko.exceptions.table;

/**
 * This exception is used when a user tries to insert an object of non-table class.
 */
public class NotTableClassException extends TableException {
    static String defaultMessage = "Not a table class.";

    public NotTableClassException(String message) {
        super(message);
    }

    public NotTableClassException() {
        super(defaultMessage);
    }
}
