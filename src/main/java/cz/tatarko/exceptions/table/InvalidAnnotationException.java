package cz.tatarko.exceptions.table;

/**
 * Exception indicated that an annotation of a forbidden type is used on a field of a table object.
 */
public class InvalidAnnotationException extends TableException {
    static String defaultMessage = "Invalid annotation.";

    public InvalidAnnotationException(String message) {
        super(message);
    }

    public InvalidAnnotationException() {
        super(defaultMessage);
    }
}
