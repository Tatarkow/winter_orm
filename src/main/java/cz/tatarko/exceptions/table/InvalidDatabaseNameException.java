package cz.tatarko.exceptions.table;

/**
 * This exception is risen when a name of database table/column contains forbidden symbols.
 */
public class InvalidDatabaseNameException extends TableException {
    static String defaultMessage = "This name cannot be used in database.";

    public InvalidDatabaseNameException(String message) {
        super(message);
    }

    public InvalidDatabaseNameException() {
        super(defaultMessage);
    }
}
