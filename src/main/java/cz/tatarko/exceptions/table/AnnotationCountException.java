package cz.tatarko.exceptions.table;

/**
 * Exception indicating that some field of a table object does not have exactly one field annotation.
 */
public class AnnotationCountException extends TableException {
    static String defaultMessage = "Each field of a table object must have exactly one field annotation.";

    public AnnotationCountException(String message) {
        super(message);
    }

    public AnnotationCountException() {
        super(defaultMessage);
    }
}