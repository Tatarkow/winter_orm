package cz.tatarko.exceptions.table;

import cz.tatarko.exceptions.WinterORMException;

/**
 * General exception for table related problems.
 */
public class TableException extends WinterORMException {
    static String defaultMessage = "Table related problem occurred.";

    public TableException(String message) {
        super(message);
    }

    public TableException() {
        super(defaultMessage);
    }
}
