package cz.tatarko.exceptions.table;

import cz.tatarko.exceptions.WinterORMException;

/**
 * This exception is thrown when a user tries to create a table that already exists.
 */
public class TableAlreadyExistsException extends WinterORMException {
    static String defaultMessage = "This table already exists.";

    public TableAlreadyExistsException(String message) {
        super(message);
    }

    public TableAlreadyExistsException() {
        super(defaultMessage);
    }
}
