package cz.tatarko;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import cz.tatarko.exceptions.ReflectionException;
import cz.tatarko.exceptions.UnknownFilterException;
import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.filters.*;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Communicates with MongoDB database.
 */
public class MongoDBHandler implements DatabaseHandler {
    private MongoDatabase database;

    @Override
    public void createTable(String tableName, Field[] fields) {
        // No need to do anything.
    }

    @Override
    public void clearDatabase() {
        MongoIterable<String> tableNames = database.listCollectionNames();

        for (String tableName: tableNames) {
            MongoCollection<Document> table = database.getCollection(tableName);
            table.drop();
        }
    }

    @Override
    public Set<String> getExistingTableNames() {
        MongoIterable<String> tableNames = database.listCollectionNames();
        Set<String> tableNameSet = new HashSet<>();

        for (String tableName: tableNames) {
            tableNameSet.add(tableName);
        }

        return tableNameSet;
    }

    @Override
    public <T extends TableObject> String insert(String tableName, T tableObject) throws WinterORMException {
        MongoCollection<Document> collection = database.getCollection(tableName);
        Document document = tableObjectToDocument(tableObject);
        collection.insertOne(document);

        return document.get("_id").toString();
    }

    private <T extends TableObject> Document tableObjectToDocument(T tableObject) throws ReflectionException {
        Field[] fields = tableObject.getClass().getDeclaredFields();
        Document document = new Document("_id", new ObjectId());

        for (Field field: fields) {
            Object value = fieldToDocumentValue(field, tableObject);
            document.append(field.getName(), value);
        }

        return document;
    }

    private <T extends TableObject> Object fieldToDocumentValue(Field field, T tableObject) throws ReflectionException {
        if (TableHelper.isTableClass(field.getType())) {
            TableObject relatedTableObject = (TableObject) Utils.getFieldValue(field, tableObject);

            if (relatedTableObject == null) {
                return null;
            }
            else {
                return new ObjectId(relatedTableObject.getID());
            }
        }
        else {
            return Utils.getFieldValue(field, tableObject);
        }
    }

    @Override
    public void connect(String name, String user, String password, String host, String port) {
        MongoClientURI uri = getURI(name, user, password, host, port);
        MongoClient mongoClient = new MongoClient(uri);
        database = mongoClient.getDatabase(name);
    }

    @Override
    public <T extends TableObject> List<T> select(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException {
        String tableName = TableHelper.getTableName(tableClass);
        MongoCollection<Document> collection = database.getCollection(tableName);
        MongoCursor<Document> cursor = getCursor(collection, filters, relatedFieldNames, tableClass);
        List<T> resultList = new ArrayList<>();

        while (cursor.hasNext()) {
            T tableObject = Utils.getInstance(tableClass);
            Document document = cursor.next();
            documentToTableObject(tableObject, document, false);
            resultList.add(tableObject);
        }

        return resultList;
    }

    private <T extends TableObject> void documentToTableObject(T tableObject, Document document, boolean isLastLevel) throws ReflectionException {
        for (Map.Entry<String, Object> pair: document.entrySet()) {
            String fieldName = pair.getKey();
            Object value = pair.getValue();

            if ("_id".equals(fieldName)) {
                tableObject.setID(value.toString());
            } else {
                Field field = Utils.getDeclaredField(tableObject.getClass(), fieldName);
                Class<?> fieldType = field.getType();

                if (TableHelper.isTableClass(fieldType)) {
                    TableObject foreignObject = Utils.getInstance((Class<? extends TableObject>) fieldType);

                    if (isLastLevel || value.getClass() == ObjectId.class) {
                        foreignObject.setLoaded(false);
                        foreignObject.setID(value.toString());
                    } else {
                        Document relatedDocument = ((List<Document>) value).get(0);
                        documentToTableObject(foreignObject, relatedDocument, true);
                    }

                    Utils.setFieldValue(field, tableObject, foreignObject);
                } else {
                    Utils.setFieldValue(field, tableObject, value);
                }
            }
        }
    }

    private <T extends TableObject> MongoCursor<Document> getCursor(MongoCollection<Document> collection, List<Filter> filters, Set<String> relatedFieldNames, Class<T> tableClass) throws WinterORMException {
        List<Bson> aggregates = new ArrayList<>();
        RelatedFieldsAugmentor.augment(relatedFieldNames, filters);

        for (String relatedFieldName: relatedFieldNames) {
            Field field = Utils.getDeclaredField(tableClass, relatedFieldName);
            String fieldName = field.getName();
            String relatedTableName = TableHelper.getTableName(field.getType());
            Bson lookup = Aggregates.lookup(relatedTableName, fieldName, "_id", fieldName);
            aggregates.add(lookup);
        }

        List<Bson> mongoFilters = getFilters(tableClass, filters);

        if (mongoFilters.size() > 0) {
            aggregates.add(Aggregates.match(Filters.and(mongoFilters)));
        }

        if (aggregates.size() > 0) {
            return collection.aggregate(aggregates).iterator();
        }
        else {
            return collection.find().iterator();
        }
    }

    private <T extends TableObject> List<Bson> getFilters(Class<T> tableClass, List<Filter> filters) throws WinterORMException {
        List<Bson> mongoFilters = new ArrayList<>();

        for (Filter filter: filters) {
            Bson mongoFilter = getFilter(tableClass, filter);
            mongoFilters.add(mongoFilter);
        }

        return mongoFilters;
    }

    private <T extends TableObject> Bson getFilter(Class<T> tableClass, Filter filter) throws WinterORMException {
        String fieldName = filter.getColumnName();
        FilterType filterType = filter.getFilterType();
        List<Object> values = filter.getValues();

        if (filterType == ForeignFilter.COMPARE) {
            return getQueryForeignCompare(tableClass, fieldName, values);
        }
        else if (filterType == IDFilter.EQUALS) {
            ObjectId id = new ObjectId((String) values.get(0));
            return Filters.eq("_id", id);
        }
        else {
            return getQuery(fieldName, filterType, values);
        }
    }

    private Bson getQuery(String fieldName, FilterType filterType, List<Object> values) throws UnknownFilterException {
        if (filterType == StringFilter.EQUALS) {
            return getEqualFilter(fieldName, values);
        }
        else if (filterType == StringFilter.IS_EMPTY) {
            return getStringFilterEmpty(fieldName, values);
        }
        else if (filterType == IntegerFilter.EQUALS) {
            return getEqualFilter(fieldName, values);
        }
        else if (filterType == IntegerFilter.LESS_THAN) {
            return Filters.lt(fieldName, values.get(0));
        }
        else if (filterType == IntegerFilter.MORE_THAN) {
            return Filters.gt(fieldName, values.get(0));
        }
        else if (filterType == IntegerFilter.IS_NULL) {
            return getNullFilter(fieldName, values);
        }
        else if (filterType == DateFilter.EQUALS) {
            return getEqualFilter(fieldName, values);
        }
        else if (filterType == DateFilter.IS_NULL) {
            return getNullFilter(fieldName, values);
        }
        else if (filterType == ForeignFilter.EQUALS) {
            return getForeignFilterEqual(fieldName, values);
        }
        else {
            throw new UnknownFilterException();
        }
    }

    private Bson getStringFilterEmpty(String fieldName, List<Object> values) {
        if (values.size() > 0 && !((boolean) values.get(0))) {
            return Filters.and(
                Filters.ne(fieldName, null),
                Filters.ne(fieldName, "")
            );
        }
        else {
            return Filters.or(
                Filters.eq(fieldName, null),
                Filters.eq(fieldName, "")
            );
        }
    }

    private Bson getEqualFilter(String fieldName, List<Object> values) {
        return Filters.eq(fieldName, values.get(0));
    }

    private Bson getForeignFilterEqual(String fieldName, List<Object> values) {
        TableObject relatedTableObject = (TableObject) values.get(0);
        String id = relatedTableObject.getID();
        return Filters.eq(fieldName, new ObjectId(id));
    }

    private <T extends TableObject> Bson getQueryForeignCompare(Class<T> tableClass, String fieldName, List<Object> values) throws WinterORMException {
        Field field = Utils.getDeclaredField(tableClass, fieldName);
        String relatedTableName = TableHelper.getTableName(field.getType());
        List<Object> copiedValues = new ArrayList<>(values);
        String relatedFieldName = (String) copiedValues.remove(0);
        FilterType relatedFilterType = (FilterType) copiedValues.remove(0);

        if (relatedFilterType == IDFilter.EQUALS) {
            ObjectId id = new ObjectId((String) copiedValues.get(0));
            String identification = String.format("%s", relatedTableName);
            return Filters.eq(identification, id);
        }
        else {
            String identification = String.format("%s.%s", relatedTableName, relatedFieldName);
            return getQuery(identification, relatedFilterType, copiedValues);
        }
    }

    private Bson getNullFilter(String fieldName, List<Object> values) {
        if (values.size() > 0 && !((boolean) values.get(0))) {
            return Filters.ne(fieldName, null);
        }
        else {
            return Filters.eq(fieldName, null);
        }
    }

    @Override
    public <T extends TableObject> void delete(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException {
        String tableName = TableHelper.getTableName(tableClass);
        MongoCollection<Document> collection = database.getCollection(tableName);

        List<Bson> mongoFilters = getFilters(tableClass, filters);
        collection.deleteMany(Filters.and(mongoFilters));
    }

    @Override
    public <T extends TableObject> void set(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters, Map<String, Object> values) throws WinterORMException {
        String tableName = TableHelper.getTableName(tableClass);
        MongoCollection<Document> collection = database.getCollection(tableName);

        List<Bson> updates = getUpdates(values);
        Bson update = Updates.combine(updates);

        List<Bson> mongoFilters = getFilters(tableClass, filters);
        Bson mongoFilter = Filters.and(mongoFilters);

        collection.updateMany(mongoFilter, update);
    }

    @Override
    public void finish() {
        // Nothing needs to be done.
    }

    private List<Bson> getUpdates(Map<String, Object> values) {
        List<Bson> updates = new ArrayList<>();

        for (String fieldName: values.keySet()) {
            Object value = values.get(fieldName);

            if (value == null) {
                updates.add(Updates.set(fieldName, null));
                continue;
            }

            if (TableHelper.isTableClass(value.getClass())) {
                String id = ((TableObject) value).getID();
                updates.add(Updates.set(fieldName, new ObjectId(id)));
            }
            else {
                updates.add(Updates.set(fieldName, value));
            }
        }

        return updates;
    }

    private MongoClientURI getURI(String name, String user, String password, String host, String port) {
        String uri = String.format("mongodb://%s:%s@%s:%s/%s", user, password, host, port, name);
        return new MongoClientURI(uri);
    }
}
