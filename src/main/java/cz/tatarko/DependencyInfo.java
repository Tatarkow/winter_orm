package cz.tatarko;

import java.util.Set;

class DependencyInfo {
    Class<TableObject> tableClass;
    Set<Class<TableObject>> dependencies;
}
