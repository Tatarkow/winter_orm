package cz.tatarko;

import cz.tatarko.exceptions.ReflectionException;
import cz.tatarko.exceptions.table.TableException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

class TableObjectHelper {
    static <T extends TableObject> T getInstance(Class<T> tableClass) throws TableException {
        try {
            return tableClass.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new TableException("Table object could not be instantiated.");
        }
    }

    static <T extends TableObject> void setValues(T tableObject, Map<String, Object> values) throws ReflectionException {
        Field[] fields = tableObject.getClass().getDeclaredFields();

        for (Field field: fields) {
            String columnName = field.getName();

            if (values.containsKey(columnName)) {
                Object value = values.get(columnName);
                Utils.setFieldValue(field, tableObject, value);
            }
        }
    }
}
