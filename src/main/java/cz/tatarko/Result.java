package cz.tatarko;

import cz.tatarko.fields.ForeignField;
import cz.tatarko.exceptions.ReflectionException;
import cz.tatarko.exceptions.WinterORMException;
import cz.tatarko.fields.OnDelete;
import cz.tatarko.filters.FilterType;
import cz.tatarko.filters.ForeignFilter;
import cz.tatarko.filters.IDFilter;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Provides access to database data of the given type. For instance it is possible to add filters to an object of this
 * class and later retrieve all the rows/objects from the database that satisfy the constraints.
 */
public class Result<T extends TableObject> {
    private static DatabaseHandler databaseHandler;
    private static WinterORM winterORM;

    private final Set<String> relatedFieldNames = new HashSet<>();
    private final List<Filter> filters = new ArrayList<>();
    private final Class<T> tableClass;

    Result(Class<T> tableClass) {
        this.tableClass = tableClass;
    }

    /**
     * Adds filter to the query. A user must provide a filter type, name of a field to which the filter shall be
     * applied and optionally also values for the filter (e.g. to what value some field has to equal).
     */
    public Result<T> filter(String columnName, FilterType filterType, Object... values) {
        Filter filter = new Filter(columnName, filterType, values);
        filters.add(filter);

        return this;
    }

    /**
     * Retrieves a single object of the given ID.
     */
    public T get(String id) throws WinterORMException {
        Filter filter = new Filter("id", IDFilter.EQUALS, id);
        filters.add(filter);

        return getList().get(0);
    }

    /**
     * Retrieves a list of objects that satisfy filter that have been provided.
     */
    public List<T> getList() throws WinterORMException {
        return databaseHandler.select(tableClass, relatedFieldNames, filters);
    }

    /**
     * Prints all the objects that satisfy filter that have been provided with respect to toString methods.
     */
    public void print() throws WinterORMException {
        for (T tableObject: this.getList()) {
            System.out.println(tableObject);
        }
    }

    /**
     * Prints all given fields of all the objects that satisfy filter that have been provided.
     */
    public void print(String... columns) throws WinterORMException {
        printHeader(columns);

        for (T tableObject: getList()) {
            printRow(tableObject, columns);
        }
    }

    private void printHeader(String[] columns) {
        StringBuilder header = new StringBuilder();

        for (String column: columns) {
            header.append(column);
            header.append("\t");
        }

        System.out.println(header);
    }

    private void printRow(T tableObject, String[] columns) throws ReflectionException {
        StringBuilder row = new StringBuilder();

        for (String column: columns) {
            Field field = Utils.getDeclaredField(tableClass, column);
            Object value = Utils.getFieldValue(field, tableObject);

            row.append(value);
            row.append("\t");
        }

        System.out.println(row);
    }

    /**
     * Efficiently retrieves related objects. For instance let's assume that a user retrieves a list of book and later
     * wants to print names of authors. Instead of doing N+1 database queries, where N is the count of the books, the
     * user could call this method a specify that the related author should be loaded to each of the books.
     */
    public Result<T> related(String... fieldNames) {
        relatedFieldNames.addAll(Arrays.asList(fieldNames));
        return this;
    }

    /**
     * Updates all the objects that satisfy filter that have been provided with the given values.
     */
    public Result<T> set(Map<String, Object> values) throws WinterORMException {
        Field[] fields = tableClass.getDeclaredFields();
        Validator.validateSet(fields, values);

        databaseHandler.set(tableClass, relatedFieldNames, filters, values);

        return this;
    }

    /**
     * Deletes all the objects that satisfy filter that have been provided. Additionally it takes care of deleting all
     * the related objects that should be deleted as well. For instance if it has been specified that a book must have
     * an author and a user will want to delete the author, the book will be automatically deleted too.
     */
    public void delete() throws WinterORMException {
        handleRelatedObjects();
        databaseHandler.delete(tableClass, relatedFieldNames, filters);
    }

    private void handleRelatedObjects() throws WinterORMException {
        List<String> deletedIDs = getList()
            .stream()
            .map(TableObject::getID)
            .collect(Collectors.toList());

        Set<Class<TableObject>> tableClasses = TableHelper.getTableClasses();

        for (Class<TableObject> otherTableClass: tableClasses) {
            handleRelatedObject(otherTableClass, deletedIDs);
        }
    }

    private void handleRelatedObject(Class<TableObject> otherTableClass, List<String> deletedIDs) throws WinterORMException {
        List<Field> fields = Arrays.stream(otherTableClass.getDeclaredFields())
            .filter(field -> field.getType() == tableClass)
            .collect(Collectors.toList());

        for (Field field: fields) {
            String fieldName = field.getName();
            ForeignField annotation = (ForeignField) TableHelper.getAnnotation(field);

            for (String id: deletedIDs) {
                Result<TableObject> result = winterORM
                    .getAll(otherTableClass)
                    .filter(fieldName, ForeignFilter.COMPARE, "id", IDFilter.EQUALS, id);

                if (annotation.onDelete() == OnDelete.DELETE) {
                    result.delete();
                }
                else if (annotation.onDelete() == OnDelete.SET_NULL) {
                    result.set(new HashMap<>() {{
                        put(fieldName, null);
                    }});
                }
            }
        }
    }

    static void setDatabaseHandler(DatabaseHandler databaseHandle) {
        Result.databaseHandler = databaseHandle;
    }

    static void setWinterORM(WinterORM winterORM) {
        Result.winterORM = winterORM;
    }
}
