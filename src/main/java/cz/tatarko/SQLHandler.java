package cz.tatarko;

import cz.tatarko.fields.DateField;
import cz.tatarko.fields.ForeignField;
import cz.tatarko.fields.IntegerField;
import cz.tatarko.fields.StringField;
import cz.tatarko.exceptions.*;
import cz.tatarko.exceptions.table.InvalidAnnotationException;
import cz.tatarko.exceptions.table.TableException;
import cz.tatarko.filters.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Handler for relational databases (e.g. PostgreSQL or H2) may be based on this abstract class.
 */
abstract class SQLHandler implements DatabaseHandler {
    protected Connection connection;

    abstract String getURL(String name, String host, String port);

    @Override
    public void createTable(String tableName, Field[] fields) throws WinterORMException {
        StringBuilder query = new StringBuilder("CREATE TABLE ");
        query.append(tableName);
        query.append(" (id SERIAL PRIMARY KEY");

        for (Field field: fields) {
            String name = field.getName();
            String datatype = getDatatype(field);
            query.append(String.format(", %s %s", name, datatype));
        }

        query.append(");");
        executeUpdate(query);
    }

    private String getDatatype(Field field) throws WinterORMException {
        Class<? extends Annotation> annotationClass = TableHelper.getAnnotationClass(field);

        if (annotationClass == IntegerField.class) {
            return "INTEGER";
        }
        else if (annotationClass == StringField.class) {
            return "TEXT";
        }
        else if (annotationClass == DateField.class) {
            return "DATE";
        }
        else if (annotationClass == ForeignField.class) {
            String name = TableHelper.getTableName(field.getType());
            return String.format("INTEGER REFERENCES %s(id)", name);
        }
        else {
            throw new InvalidAnnotationException("Unknown annotation used on a field of a table object.");
        }
    }


    private void executeUpdate(String query) throws ORMSQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    void executeUpdate(StringBuilder query) throws ORMSQLException {
        executeUpdate(query.toString());
    }

    abstract String getQueryForExistingTableNames();

    @Override
    public Set<String> getExistingTableNames() throws ObtainingExistingTableNamesException {
        String query = getQueryForExistingTableNames();

        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            Set<String> tableNames = new HashSet<>();

            while (resultSet.next()) {
                String tableName = resultSet.getString("table_name").toLowerCase();
                tableNames.add(tableName);
            }

            return tableNames;
        } catch (SQLException e) {
            throw new ObtainingExistingTableNamesException();
        }
    }

    @Override
    public <T extends TableObject> String insert(String tableName, T tableObject) throws WinterORMException {
        Field[] fields = tableObject.getClass().getDeclaredFields();
        List<String> fieldNames =  Arrays.stream(fields).map(Field::getName).collect(Collectors.toList());

        StringBuilder query = new StringBuilder(String.format("INSERT INTO %s (", tableName));
        query.append(String.join(", ", fieldNames));
        query.append(") VALUES (");
        query.append("?, ".repeat(fields.length-1));
        query.append("?);");

        try (PreparedStatement statement = connection.prepareStatement(query.toString(), Statement.RETURN_GENERATED_KEYS)) {
            for (int i=0; i<fields.length; i++) {
                Field field = fields[i];
                Object value = Utils.getFieldValue(field, tableObject);
                setValue(statement, value, field, i+1);
            }

            statement.executeUpdate();
            return getInsertedID(statement);
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private String getInsertedID(PreparedStatement statement) throws SQLException {
        ResultSet resultSet = statement.getGeneratedKeys();

        if (resultSet.next()){
            int id = resultSet.getInt(1);
            return Integer.toString(id);
        }

        throw new SQLException();
    }

    @Override
    public void connect(String name, String user, String password, String host, String port) throws ConnectionException {
        String url = getURL(name, host, port);
        connection = DataSource.getConnection(url, user, password);
    }

    @Override
    public <T extends TableObject> void delete(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException {
        StringBuilder query = new StringBuilder();
        prepareDeleteFields(query, tableClass);
        prepareFilters(query, tableClass, filters);

        try (PreparedStatement statement = connection.prepareStatement(query.toString())) {
            applyFilters(statement, filters);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private <T extends TableObject> void prepareDeleteFields(StringBuilder query, Class<T> tableClass) {
        String tableName = TableHelper.getTableName(tableClass);
        query.append(String.format("DELETE FROM %s ", tableName));
    }

    @Override
    public <T extends TableObject> void set(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters, Map<String, Object> values) throws WinterORMException {
        RelatedFieldsAugmentor.augment(relatedFieldNames, filters);
        Set<Field> relatedFields = getRelatedFields(relatedFieldNames, tableClass);

        StringBuilder query = new StringBuilder();
        prepareSetFields(query, tableClass, relatedFields, values);
        prepareFilters(query, tableClass, filters);

        try (PreparedStatement statement = connection.prepareStatement(query.toString())) {
            applyValues(statement, tableClass, values);
            applyFilters(statement, filters, values.size());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private <T extends TableObject> void applyValues(PreparedStatement statement, Class<T> tableClass, Map<String, Object> values) throws WinterORMException {
        int index = 0;

        for (String column: values.keySet()) {
            index++;

            Field field = Utils.getDeclaredField(tableClass, column);
            Object value = values.get(column);
            setValue(statement, value, field, index);
        }
    }

    private void setDate(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value == null) {
            statement.setNull(index, Types.DATE);
        }
        else {
            long timestamp = ((java.util.Date) value).getTime();
            statement.setDate(index, new java.sql.Date(timestamp));
        }
    }

    private void setInteger(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value == null) {
            statement.setNull(index, Types.INTEGER);
        }
        else {
            statement.setInt(index, (Integer) value);
        }
    }

    private void setString(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value == null) {
            statement.setNull(index, Types.VARCHAR);
        }
        else {
            statement.setString(index, (String) value);
        }
    }

    private void setForeign(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value == null) {
            statement.setNull(index, Types.INTEGER);
        }
        else {
            String id = ((TableObject) value).getID();
            statement.setInt(index, Integer.parseInt(id));
        }
    }

    private <T extends TableObject> void prepareSetFields(StringBuilder query, Class<T> tableClass, Set<Field> relatedFields, Map<String, Object> values) {
        String tableName = TableHelper.getTableName(tableClass);
        query.append(String.format("UPDATE %s SET ", tableName));

        List<String> valuePairs = new ArrayList<>();

        for (String column: values.keySet()) {
            String valuePair = String.format("%s = ?", column);
            valuePairs.add(valuePair);
        }

        query.append(String.join(", ", valuePairs));
        query.append(" ");
    }

    private <T extends TableObject> Set<Field> getRelatedFields(Set<String> relatedFieldNames, Class<T> tableClass) throws ReflectionException {
        Set<Field> relatedFields = new HashSet<>();

        for (String fieldName : relatedFieldNames) {
            Field field = Utils.getDeclaredField(tableClass, fieldName);
            relatedFields.add(field);
        }

        return relatedFields;
    }

    @Override
    public <T extends TableObject> List<T> select(Class<T> tableClass, Set<String> relatedFieldNames, List<Filter> filters) throws WinterORMException {
        RelatedFieldsAugmentor.augment(relatedFieldNames, filters);
        Set<Field> relatedFields = getRelatedFields(relatedFieldNames, tableClass);

        StringBuilder query = new StringBuilder();
        prepareSelectFields(query, tableClass, relatedFields);
        prepareFilters(query, tableClass, filters);

        try (PreparedStatement statement = connection.prepareStatement(query.toString())) {
            applyFilters(statement, filters);
            ResultSet resultSet = statement.executeQuery();
            return resultSetToTableObjectList(resultSet, tableClass, relatedFields);
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private <T extends TableObject> void prepareSelectFields(StringBuilder query, Class<T> tableClass, Set<Field> relatedFields) {
        String tableName = TableHelper.getTableName(tableClass);

        List<String> allFields = new ArrayList<>();
        allFields.add(String.format("%s.id AS %s_id", tableName, tableName));

        for (Field field: tableClass.getDeclaredFields()) {
            String fieldName = field.getName();
            allFields.add(String.format("%s.%s AS %s_%s", tableName, fieldName, tableName, fieldName));
        }

        for (Field field: relatedFields) {
            Class<?> relatedType = field.getType();
            String relatedTableName = TableHelper.getTableName(relatedType);
            allFields.add(String.format("%s.id AS %s_id", relatedTableName, relatedTableName));

            for (Field foreignField: relatedType.getDeclaredFields()) {
                String fieldName = foreignField.getName();
                allFields.add(String.format("%s.%s AS %s_%s", relatedTableName, fieldName, relatedTableName, fieldName));
            }
        }

        query.append("SELECT ");
        query.append(String.join(", ", allFields));
        query.append(String.format(" FROM %s ", tableName));

        addJoin(relatedFields, tableName, query);
    }

    private void addJoin(Set<Field> relatedFields, String tableName, StringBuilder query) {
        for (Field field: relatedFields) {
            String fieldName = field.getName();
            String fieldType = TableHelper.getTableName(field.getType());
            query.append(String.format("INNER JOIN %s ON %s.%s = %s.id ", fieldType, tableName, fieldName, fieldType));
        }
    }

    private <T extends TableObject> List<T> resultSetToTableObjectList(ResultSet resultSet, Class<T> tableClass, Set<Field> relatedFields) throws WinterORMException {
        List<T> resultList = new ArrayList<>();

        try {
            while (resultSet.next()) {
                T object = TableObjectHelper.getInstance(tableClass);
                resultSetToTableObject(object, resultSet, relatedFields, false);
                resultList.add(object);
            }

        } catch (SQLException e) {
            throw new ORMSQLException();
        }

        return resultList;
    }

    private <T extends TableObject> void resultSetToTableObject(T object, ResultSet resultSet, Set<Field> relatedFields, boolean isLastLevel) throws SQLException, TableException, ReflectionException {
        Class<?> tableClass = object.getClass();
        String tableName = TableHelper.getTableName(tableClass);
        Field[] fields = tableClass.getDeclaredFields();

        Field idField = Utils.getDeclaredField(TableObject.class, "id");
        Integer id = resultSet.getInt(String.format("%s_id", tableName));
        Utils.setFieldValue(idField, object, id.toString());

        for (Field field: fields) {
            Class<?> fieldType = field.getType();
            String fieldName = field.getName();
            String identification = String.format("%s_%s", tableName, fieldName);
            Object value = getObjectFromResultSet(resultSet, identification);

            if (TableHelper.isTableClass(fieldType)) {
                if (value == null) {
                    Utils.setFieldValue(field, object, null);
                    continue;
                }

                String relatedTableName = TableHelper.getTableName(fieldType);
                TableObject foreignObject = TableObjectHelper.getInstance((Class<? extends TableObject>) fieldType);

                if (!isLastLevel && isInRelatedFields(relatedFields, relatedTableName)) {
                    resultSetToTableObject(foreignObject, resultSet, relatedFields, true);
                }
                else {
                    foreignObject.setLoaded(false);
                }
                foreignObject.setID(value.toString());
                Utils.setFieldValue(field, object, foreignObject);
            }
            else {
                Utils.setFieldValue(field, object, value);
            }
        }
    }

    protected abstract Object getObjectFromResultSet(ResultSet resultSet, String identification) throws SQLException;

    private boolean isInRelatedFields(Set<Field> relatedFields, String relatedTableName) {
        return relatedFields
            .stream()
            .anyMatch(field -> TableHelper
                .getTableName(field.getType())
                .equals(relatedTableName)
            );
    }

    private void applyFilters(PreparedStatement statement, List<Filter> filters) throws WinterORMException {
        applyFilters(statement, filters, 0);
    }
    private void applyFilters(PreparedStatement statement, List<Filter> filters, int startIndex) throws WinterORMException {
        int counter = 0;
        int index = startIndex;

        while (counter < filters.size()) {
            Filter filter = filters.get(counter);
            List<Object> values = filter.getValues();
            FilterType filterType = filter.getFilterType();

            if ("id".equals(filter.getColumnName())) {
                integerIntoStatement(statement, index+1, Integer.parseInt((String) values.get(0)));
            }
            else {
                valueIntoStatement(index+1, filterType, statement, values);
            }

            index++;
            counter++;
        }
    }

    private void valueIntoStatement(int index, FilterType filterType, PreparedStatement statement, List<Object> values) throws WinterORMException {
        if (filterType == StringFilter.EQUALS) {
            stringIntoStatement(statement, index, values.get(0));
        }
        else if (filterType == StringFilter.IS_EMPTY) {
            // Nothing needs to be done.
        }
        else if (filterType == IntegerFilter.EQUALS) {
            integerIntoStatement(statement, index, values.get(0));
        }
        else if (filterType == IntegerFilter.LESS_THAN) {
            integerIntoStatement(statement, index, values.get(0));
        }
        else if (filterType == IntegerFilter.MORE_THAN) {
            integerIntoStatement(statement, index, values.get(0));
        }
        else if (filterType == IntegerFilter.IS_NULL) {
            // Nothing needs to be done.
        }
        else if (filterType == DateFilter.EQUALS) {
            dateIntoStatement(statement, index, values.get(0));
        }
        else if (filterType == DateFilter.IS_NULL) {
            // Nothing needs to be done.
        }
        else if (filterType == ForeignFilter.EQUALS) {
            integerIntoStatement(statement, index, Integer.parseInt(((TableObject) values.get(0)).getID()));
        }
        else if (filterType == IDFilter.EQUALS) {
            integerIntoStatement(statement, index, Integer.parseInt((String) values.get(0)));
        }
        else if (filterType == ForeignFilter.COMPARE) {
            List<Object> copiedValues = new ArrayList<>(values);
            copiedValues.remove(0);
            FilterType foreignFilterType = (FilterType) copiedValues.remove(0);
            valueIntoStatement(index, foreignFilterType, statement, copiedValues);
        }
        else {
            throw new UnknownFilterException();
        }
    }

    private void dateIntoStatement(PreparedStatement statement, int index, Object value) throws ORMSQLException {
        long timestamp = ((java.util.Date) value).getTime();

        try {
            statement.setDate(index, new java.sql.Date(timestamp));
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private void integerIntoStatement(PreparedStatement statement, int index, Object value) throws ORMSQLException {
        try {
            statement.setInt(index, (Integer) value);
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private void stringIntoStatement(PreparedStatement statement, int index, Object value) throws ORMSQLException {
        try {
            statement.setString(index, (String) value);
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    private <T extends TableObject> void prepareFilters(StringBuilder query, Class<T> tableClass, List<Filter> filters) throws WinterORMException {
        if (filters.size() == 0) {
            return;
        }

        query.append("WHERE ");

        for (Filter filter: filters.subList(0, filters.size()-1)) {
            query.append(SQLFilterHelper.filterToQuery(tableClass, filter));
            query.append("AND ");
        }

        Filter lastFilter = filters.get(filters.size()-1);
        query.append(SQLFilterHelper.filterToQuery(tableClass, lastFilter));
    }

    private void setValue(PreparedStatement statement, Object value, Field field, int index) throws WinterORMException {
        try {
            Class<?> fieldType = field.getType();

            if (fieldType == String.class) {
                setString(statement, index, value);
            }
            else if (fieldType == Integer.class) {
                setInteger(statement, index, value);
            }
            else if (fieldType == java.util.Date.class) {
                setDate(statement, index, value);
            }
            else if (TableHelper.isTableClass(fieldType)) {
                setForeign(statement, index, value);
            }
            else {
                throw new TableException();
            }
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }

    public void finish() throws ORMSQLException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new ORMSQLException();
        }
    }
}
