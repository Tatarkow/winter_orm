# WinterORM
## Introduction
WinterORM is a simple object-relational mapping written in Java. It has been developed as a university project. There is no hidden meaning in the name; it was just implemented in winter.

Usage should be clear from the tests and various examples explained below. For more information check the Javadoc included in the code itself.

## Requirements
WinterORM requires Java 14 and the project uses Maven. WinterORM supports three databases - PostgreSQL, MongoDB and H2. The latest uses a file to actually store the data, hence it does not need to be installed. On the other hand, in order to uses to former two databases it is necessary to install them in advance.

## Makefile
The project includes Makefile.
* `make test` executes all the tests using the current database (see later).
* `make full_test` executes all the tests using all the supported databases. Note that all of them must be then installed.
* `make setup_postgresql` creates an empty database (named `winter_orm_database`) for PostgresSQL.
* `make setup_mongo` creates an empty database (named `winter_orm_database`) for MongoDB.
* `make setup` creates an empty database (named `winter_orm_database`) for PostgresSQL and MongoDB.
* `make clear_postgresql` drops the database (named `winter_orm_database`) in PostgreSQL.
* `make clear_mongo` drops the database (named `winter_orm_database`) in MongoDB.
* `make clear` drops the database (named `winter_orm_database`) in PostgreSQL and MongoDB.

## What WinterORM can do?
### Basic concepts
A user should start by defining a database structure. This can be achieved by defining classes that will represent database tables and whose fields will correspond with the columns of the tables.

Then a user can define which database shall be used. This can be done by changing the content of `./src/main/resources/META-INF/services/cz.tatarko.DatabaseHandler` file to one of the following.
* `cz.tatarko.H2Handler` to use H2.
* `cz.tatarko.PostgreSQLHandler` to use PostgreSQL.
* `cz.tatarko.MongoDBHandler` to use MongoDB.

Then it is possible to instantiate the `WinterORM` class like this.  
```java
String packageName = "cz.tatarko.tables";
String name = "winter_orm_database";
String user = "winter_orm_user";
String password = "winter_orm_password";
String host = "localhost";
String port = "5432";

WinterORM winterORM = new WinterORM(packageName, name, user, password, host, port);
```
The `packageName` is package containing all the classes specifying the database structure.

With `winterORM` it is then possible to communicate with the database. For instance, one can list all the data. After we are finish, `finish` method on `winterORM` should be called.

### How to define tables
Database tables can be defined using so-called table classes, i.e. classes that inherit from `TableObject` class. For each column it is necessary to define a field. Examples may be found in the tests.

The type of the column corresponds to the type of database column. Therefore, it is only possible to have there fields of type `Integer`, `String`, `Date` or a table class. To make it more explicit and provide a way for passing additional information, each field must have an appropriate annotation. Use
* `@IntegerField` for `Integer` fields,
* `@StringField` for `String` fields,
* `@DateField` for `Date` fields,
* `@ForeignField` for table class fields.

All the field annotation have `nullable` property. Its default is `true`. If you want to define a field that should never be `null`, set this to `false`. Additionally, `StringField` has `maxLength` property, which allows to limit the maximal length of the string. The default value is `0`, which indicates unlimited length. Finally, `ForeignField` has additionally `onDelete`. It can be set either to `SET_NULL`, or to `DELETE`. For instance, if `Book` has `Author` foreign field you may specify what should happen with the book, once the author gets deleted. The default is `SET_NULL`, which will assign `null` to the `Author` field. The other option, `DELETE`, would delete the book as well. 

### How to create tables
Once the table objects are defined and `winterORM` object of class `WinterORM` has been created it is possible to create the tables in the database using the following command.
```java
winterORM.createTables();
```

In the following section we will assume that the database tables have been already created.

### How to insert data
To inserting new data to the database, just call the `insert` method and specify the database table and the data itself.

```java
Author shakespeare = winterORM.insert(Author.class, Map.of(
    "name", "William",
    "surname", "Shakespeare"
));
```

Objects with foreign fields may be inserted in the same fashion like this

```java
Book othello = winterORM.insert(Book.class, Map.of(
    "author", shakespeare,
    "title", "Othello",
    "publicationYear", 1604
));
``` 

### How to retrieve data
To retrieve data one must first specify the table using `getAll` method. On the result (of class `Result`) it is then possible to call `getList` method to obtain all the rows in the specified database table.
```java
List<Author> authors = winterORM.getAll(Author.class).getList();
```

Optionally it is possible to retrieve only a single object based on its ID using `get` method.
```java
Author retrivedShakespeare = winterORM.getAll(Author.class).get(shakespeare.getID());
```

### How to print data
To print the data it is first recommended to define `toString` method the table objects. For instance here is the method for `Book` from the tests.
```java
@Override
public String toString() {
    return String.format("[BOOK] { %s written by %s in %s }", title, author, publicationYear);
}
```

Then it is possible to print the retrieved data using `print` method like this.
```java
winterORM.getAll(Author.class).print();
```
The result may look like this.
```text
[AUTHOR] { William Shakespeare }
[AUTHOR] { Molier }
```

Alternatively, it is possible to specify which columns should be printed. For instance this
```java
winterORM.getAll(Author.class).print("name", "surname");
``` 
may produce something like this
```text
name	surname	
Molier	null	
Jan	Komensky
```
Note that the `middleName` (e.g. `Amos`) is not present and `null` is printed.

### How to filter data
It is possible to conveniently filter the retrieved data using `filter` method. Let us begin by an example, which would return only authors whose name is equal to `Jan`.
```java
winterORM
    .getAll(Author.class)
    .filter("name", StringFilter.EQUALS, "Jan")
    .getList();
```

In general the first argument specifies the name of the field to which the filter shall be applied. The second argument then defines the rule that will be applied. All the other fields serve for providing values.

For fields of type `StringField` it is possible to use either `StringFilter.EQUALS` or `StringFilter.IS_EMPTY`. The first one takes one argument, which is used for a perfect match. The second filter takes one optional boolean argument. If no argument is provided, it behaves like `true` was provided. It check whether the field is either `null` or an empty string.

For fields of type `IntegerField` it is possible to use `IntegerFilter.EQUALS`, `IntegerFilter.LESS_THAN`, `IntegerFilter.MORE_THAN` or `IntegerFilter.IS_NULL`. Filter `IntegerFilter.EQUALS` takes one argument and tests whether value in the database is equal to the provided value. Filters `IntegerFilter.LESS_THAN` and `IntegerFilter.MORE_THAN` behave analogically. Filter `IntegerFilter.IS_NULL` takes one optional boolean argument (the default is `true`) and check whether the value in the database is `null`.

For fields of type `DateField` it is possible to use `DateField.EQUALS` or `DateField.IS_NULL`. Both of them behave similarly to the integer filters.

For fields of type `ForeignField` it is possible to sue `ForeignField.EQUALS` or `ForeignField.COMPARE`. The former one is similar to other `EQUALS` filters. The later one is used for filtering based on a foreign object. For instance it is possible to filter books base on the `name` field of the related author. Let's first see an example.
```java
winterORM
    .getAll(Book.class)
    .filter("author", ForeignFilter.COMPARE, "name", StringFilter.EQUALS, "William")
    .getList();
```
The first two parameters are - as always - field name and filter type, respectfully. The third argument is then name of the related field. It is followed by filter type that should be applied to the related field. The rest of the values will be passed to the second filter as its values. Note that it is not possible to use `COMPARE` filter as the related filter.

Of course, the filters may be combined. Here is an example of it.
```java
winterORM
    .getAll(Book.class)
    .filter("publicationYear", IntegerFilter.LESS_THAN, 1650)
    .filter("publicationYear", IntegerFilter.MORE_THAN, 1600)
    .getList();
```

### How to modify data
Filtered records may be modified using `set` method, which takes a map of field names an new values.
```java
.getAll(Book.class)
    .filter("publicationYear", IntegerFilter.MORE_THAN, 1600)
    .set(Map.of("publicationYear", 2000))
    .getList();
``` 

Alternatively it is possible to call `set` directly on the table object.
```java
shakespeare.set(Map.of("name", "Billy"));
```

### How to delete data
Filtered records may be deleted using `delete` method.
```java
.getAll(Book.class)
    .filter("publicationYear", IntegerFilter.LESS_THAN, 1600)
    .delete()
``` 

Alternatively it is possible to call `delete` directly on the table object.
```java
shakespeare.delete();
```

### How to refresh data
After the data has been update in the database it is not guaranteed that the new values will be in the loaded table object. To update it, it is necessary to call `refresh` method.
```java
shakespeare.refresh();
``` 

### How to prefetch data
If we for instance wanted to print `name` of all authors that wrote the retrieved books, we would need `N+1` queries, where `N` is number of the books (one for the list of books, i.e. also a list of author IDs and then one for each related author). To optimized it we can prefetch the authors using `related` method. In other words the authors will be immediately loaded. Note that this is implicitly done when using the `ForeignFilter.COMPARE` filter. The method can take multiple field names of related table objects that should be loaded.
```java
winterORM
    .getAll(Friend.class)
    .related("favoriteAuthor", "favoriteBook")
    .getList();
```

## How is WinterORM structured?
As it has been already said, `WinterORM` is the main class of this application. Once `getAll` method is called, `Result` object is returned. This object then provides API for working with the database (`getList`, `delete` etc.). `TableObject` class is the base class for user-defined table objects. It also contains similar API to `Result`, which e.g enables deleting a single object easily.

Because each database has different API, there is a handler between `Result` and the database itself. Handler is a class that implement `DatabaseHandler` interface. It has methods like `insert`, `select` and `delete`.  This application contains `H2Handler`, `PostgreSQLHandler` and `MongoDBHandler`.

There is a lot of exceptions. All of them inherit from `WinterORM` exception and are located in `exceptions/`. Field annotations are inside `field/`. Filters are placed in `filters/`. `TableObjectValidator` class checks that the table objects are well-defined. `Validator` class make sure that valid values are used for inserting and updating data.

There are several helper classes. `Utils` contains general functions for simplifying the syntax elsewhere. `TableHelper` contains general function for working with the database tables (e.g. obtaining the table classes). `TableCreator` takes care of creating tables. `TableObjectHelper` contains helper function for working with table objects. `SQLFilterHelper` provides helper functions for SQL handlers, i.e.`H2Handler` and `PostgreSQLHandler`. `DependencyInfo` is just a simple helper data structure. `DataSource` simplifies connection itself to the database. `DatabaseHandlerHelper` takes care of setting the database handler based on `cz.tatarko.DatabaseHandler` file. `RelatedFieldsAugmentor` class augments the list of related table objects that need to be prefetch because of `ForeignField.COMPARE` filter.
